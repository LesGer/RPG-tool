package com.atolsoft.rpgtool.mainWindow.GUI.GUITabs.InvectoryWindow;

import com.atolsoft.rpgtool.data.Inventory.InventoryList;
import com.atolsoft.rpgtool.mainWindow.GUI.GUICreatorHelpers.GUICharCreatorHelper;
import com.atolsoft.rpgtool.data.Inventory.ArmorsEnum;
import com.atolsoft.rpgtool.mainWindow.GUI.Listeners.InventoryFrame.InventoryButtonsListener;
import com.atolsoft.rpgtool.mainWindow.GUI.Listeners.InventoryFrame.ComboBoxItemListener;
import com.atolsoft.rpgtool.mainWindow.GUI.Listeners.InventoryFrame.ListinerFactory;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import java.awt.*;

public class GUIInventoryFrame {

    private JFrame frame;
    private InventoryList inventory;
    private DefaultTableModel tableModel;
    private ComboBoxItemListener inventoryListener;

    public GUIInventoryFrame() {
        this.inventoryListener = new ComboBoxItemListener();
        this.inventory = new InventoryList();
        this.frame = new JFrame("Inventory Panel");
        this.frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        this.frame.setPreferredSize(new Dimension(500, 500));
        this.frame.setMinimumSize(new Dimension(500, 500));
        this.frame.setMaximumSize(new Dimension(500, 500));
        this.frame.add(mainFrame());
        this.frame.pack();
        this.frame.setVisible(true);
    }

    private JPanel mainFrame() {
        JPanel panel = new JPanel();
        panel.setLayout(null);


        panel.add(itemTable(15, 25));

        panel.add(GUICharCreatorHelper.longLabelInitialization("Armors", 325, 25));
        panel.add(itemComboBox(325, 50));
        panel.add(invButton("Add", 325, 80, InventoryButtonsListener.ADD_ARMOR));
        panel.add(invButton("Remove", 325, 180, InventoryButtonsListener.REMOVE));
        panel.add(invButton("OK", 325, 410, InventoryButtonsListener.OK));
        panel.add(GUICharCreatorHelper.longLabelInitialization("Total weight", 15, 410));
        return panel;
    }

    private JButton invButton(String text, int x, int y, String actionCommand) {
        JButton button = new JButton(text);
        button.setBounds(x, y, 150, 25);
        button.setFont(GUICharCreatorHelper.skillFont());
        button.addActionListener(ListinerFactory.factory(actionCommand, inventory,
                tableModel, inventoryListener));
        button.setActionCommand(actionCommand);
        return button;
    }

    private JComboBox<String> itemComboBox(int x, int y) {
        JComboBox<String> comboBox = new JComboBox<>();
        comboBox.setBounds(x, y, 150, 25);
        comboBox.setFont(GUICharCreatorHelper.skillFont());
        comboBox.addItemListener(inventoryListener);

        for (int i = 0; i < ArmorsEnum.values().length; i++) {
            comboBox.addItem(ArmorsEnum.values()[i].getArmorName());
        }

        return comboBox;
    }

    private JScrollPane itemTable(int x, int y) {
        String[] tableInfo = {"No", "Items", "Count", "Type", "Weight"};
        Object[][] tableContent = {{1, "Dupa", 1, "Armor", "10 kg"}};
        tableModel = new DefaultTableModel(tableContent, tableInfo);
        JTable table = new JTable(tableModel);
        table.setFillsViewportHeight(true);
        setColumnWidth(table, 0, 25);
        setColumnWidth(table, 2, 50);
        JScrollPane scrollPane = new JScrollPane(table);
        scrollPane.setBounds(x, y, 300, 380);
        return scrollPane;
    }



    private void setColumnWidth(JTable table, int columnNo, int width) {
        TableColumn column = table.getColumnModel().getColumn(columnNo);
        column.setMaxWidth(width);
        column.setPreferredWidth(width);
    }
}
