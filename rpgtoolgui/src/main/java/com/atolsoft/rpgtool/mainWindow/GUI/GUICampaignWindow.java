package com.atolsoft.rpgtool.mainWindow.GUI;

import com.atolsoft.rpgtool.data.CharacterDatabase;
import com.atolsoft.rpgtool.mainWindow.GUI.GUITabs.GUICharacterTab;
import com.atolsoft.rpgtool.mainWindow.GUI.Listeners.AddCharacterListener;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class GUICampaignWindow{
    private JFrame mainWindow;
    private CharacterDatabase characterDatabase;
    private GUICharacterTab characterTab;

    public GUICampaignWindow() {
        this.characterDatabase = new CharacterDatabase();
        this.mainWindow = new JFrame();
        windowInitializer();
        createMenuBar();
        createTabedPannel();

    }

    private void windowInitializer() {
        mainWindow.setMinimumSize(new Dimension(1280, 720));
        mainWindow.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        mainWindow.pack();
        mainWindow.setVisible(true);
    }

    private void createMenuBar() {
        JMenuBar menuBar = new JMenuBar();
        menuBar.add(fileMenu());
        menuBar.add(charactersMenu());
        menuBar.add(npcMenu());
        menuBar.add(locationMenu());
        menuBar.add(bestiary());
        menuBar.add(helpMenu());

        mainWindow.setJMenuBar(menuBar);
    }

    private JMenu fileMenu() {
        JMenu file = new JMenu("File");
        JMenuItem newItem = new JMenuItem("New");
        JMenuItem addItem = new JMenuItem("Add to campain");
        JMenuItem exit = new JMenuItem("Exit");

        file.add(newItem);
        file.add(addItem);
        file.addSeparator();
        file.add(exit);

        return file;
    }

    private JMenu charactersMenu() {
        JMenu characterMenu = new JMenu("Characters");
        JMenuItem newChar = characterMenuItem("Add",new AddCharacterListener(characterDatabase));
        JMenuItem remChar = new JMenuItem("Remove new character");

        characterMenu.add(newChar);
        characterMenu.addSeparator();
        characterMenu.add(remChar);

        return characterMenu;
    }

    private JMenuItem characterMenuItem(String text, ActionListener listener) {
        JMenuItem item = new JMenuItem(text);
        item.addActionListener(listener);
        item.setActionCommand(text);
        return item;
    }

    private JMenu npcMenu() {
        JMenu npcMenu = new JMenu("NPC");
        JMenuItem newNPC = new JMenuItem("Add new NPC");
        JMenuItem editNPC = new JMenuItem("Edit NPC");
        JMenuItem importNPC = new JMenuItem("Import NPC");
        JMenuItem remNPC = new JMenuItem("Remove NPC");

        npcMenu.add(newNPC);
        npcMenu.add(editNPC);
        npcMenu.add(importNPC);
        npcMenu.addSeparator();
        npcMenu.add(remNPC);

        return npcMenu;
    }

    private JMenu bestiary() {
        JMenu bestiary = new JMenu("Bestiary");
        JMenuItem viewBestiary = new JMenuItem("View bestiary");
        JMenuItem addBeast = new JMenuItem("Add beast");
        JMenuItem importBeast = new JMenuItem("Import beast");
        JMenuItem removeBeast = new JMenuItem("Remove beast");

        bestiary.add(viewBestiary);
        bestiary.add(addBeast);
        bestiary.add(importBeast);
        bestiary.addSeparator();
        bestiary.add(removeBeast);

        return bestiary;
    }

    private JMenu locationMenu() {
        JMenu locationMenu = new JMenu("Locations");
        JMenuItem newLocation = new JMenuItem("Add new location");
        JMenuItem editLocation = new JMenuItem("Edit location");
        JMenuItem importLocation = new JMenuItem("Import location");
        JMenuItem removeLocation = new JMenuItem("Remove location");

        locationMenu.add(newLocation);
        locationMenu.add(editLocation);
        locationMenu.add(importLocation);
        locationMenu.addSeparator();
        locationMenu.add(removeLocation);

        return locationMenu;
    }

    private JMenu helpMenu() {
        JMenu helpMenu = new JMenu("Help");
        JMenuItem help = new JMenuItem("Help");
        JMenuItem about = new JMenuItem("About");
        helpMenu.add(help);
        helpMenu.add(about);

        return helpMenu;
    }

    private void createTabedPannel() {
        JTabbedPane mainTabbedPane = new JTabbedPane();


        JComponent panel2 = makeTextPanel("Tab2");
        JComponent panel3 = makeTextPanel("Tab3");
        JComponent panel4 = makeTextPanel("Tab4");

        mainTabbedPane.addTab("Characters", new JScrollPane(characterTab = new GUICharacterTab(characterDatabase)));
        mainTabbedPane.addTab("NPC", panel2);
        mainTabbedPane.addTab("Locations", panel3);
        mainTabbedPane.addTab("Combat", panel4);


        mainWindow.add(mainTabbedPane);

    }

    protected JComponent makeTextPanel(String text) {
        //TODO przeanalizuj kod
        JPanel panel = new JPanel(false);
        JLabel filler = new JLabel(text);
        filler.setHorizontalAlignment(JLabel.CENTER);
        panel.setLayout(new GridLayout(1, 1));
        panel.add(filler);
        return panel;
    }
}
