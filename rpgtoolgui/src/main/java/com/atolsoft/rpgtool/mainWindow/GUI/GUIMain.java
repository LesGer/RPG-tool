package com.atolsoft.rpgtool.mainWindow.GUI;

import com.atolsoft.rpgtool.mainWindow.GUI.GUITabs.InvectoryWindow.GUIInventoryFrame;

public class GUIMain {
    public static void main(String[] args) {
        javax.swing.SwingUtilities.invokeLater(new Runnable() { //fragment
            public void run() { // kodu odpowiedzialny za umieszczenie wywołania
                // w specjalnym dla Swinga wątku event dispatcher
                new GUIInventoryFrame();
            }
        });
    }
}

