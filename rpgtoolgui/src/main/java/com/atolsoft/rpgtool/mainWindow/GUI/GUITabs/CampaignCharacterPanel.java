package com.atolsoft.rpgtool.mainWindow.GUI.GUITabs;

import com.atolsoft.rpgtool.data.FalloutCharacter;
import com.atolsoft.rpgtool.data.Inventory.ArmorsEnum;
import com.atolsoft.rpgtool.data.Special;
import com.atolsoft.rpgtool.data.names.SecondaryStatsName;
import com.atolsoft.rpgtool.data.names.SkillsName;
import com.atolsoft.rpgtool.data.names.SpecialNames;
import com.atolsoft.rpgtool.data.skills.Skills;
import com.atolsoft.rpgtool.mainWindow.GUI.GUICreatorHelpers.GUICharCreatorHelper;
import com.atolsoft.rpgtool.mainWindow.GUI.Listeners.AddCharacterListener;
import com.atolsoft.rpgtool.mainWindow.GUI.Listeners.InventoryListener;
import com.atolsoft.rpgtool.util.traits.traits_specific.Trait;


import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class CampaignCharacterPanel extends JPanel {

    private FalloutCharacter character;
    private ActionListener newCharacterListener, invectoryListiner;

    public CampaignCharacterPanel(FalloutCharacter character){
        super();
        this.character = character;
        invectoryListiner = new InventoryListener();
        characterPaneInitialization();
    }

    public CampaignCharacterPanel(AddCharacterListener listener){
        super();
        this.newCharacterListener = listener;
        emptyPanelInitialization();
    }

    // TODO poprawić to
    private void characterPaneInitialization(){
        this.setBorder(GUICharCreatorHelper.borerToPane(borderName()));
        panelProperties();
        specialLabelsCreator();
        secStatsLabelCreator();
        skillsLabelCreator();
        itemsComponentsSetter();
        traitsAndPerks();
        inventorySetter();
        //xpAndModsComponents();
    }
    // TODO i to
    private void panelProperties() {
        this.setLayout(null);
        this.setMinimumSize(new Dimension(600,500));
        this.setMaximumSize(new Dimension(600,500));
        this.setPreferredSize(new Dimension(600,500));
    }

    private void emptyPanelInitialization() {
        panelProperties();
        this.setBorder(GUICharCreatorHelper.borerToPane("ADD CHARACTER"));
        this.add(buttonInitializer(200,250, newCharacterListener));


    }

    private String borderName(){
        StringBuilder builder  = new StringBuilder();
        builder.append(character.getName());
        builder.append(" - ");
        builder.append(" Age: ");
        builder.append(character.getAge());
        builder.append(" - ");
        builder.append(" LEVEL ");
        builder.append(character.getLevel());
        return builder.toString();
    }

    private void specialLabelsCreator() {
        add(GUICharCreatorHelper.longLabelInitialization("SPECIAL", 15, 25));
        String specialName;
        Integer value;
        Iterator<Map.Entry<String, Special>> specialIterator = character.getSpecial().entrySet().iterator();
        for (int i = 0, x = 15, y = 45; i < character.getSpecial().size(); i++, x+=22) {
            specialName = SpecialNames.values()[i].getSpecialKey();
            add(specialLabels(x,y,specialName));
            Map.Entry<String, Special> element = specialIterator.next();
            value = element.getValue().getSpecialValue();
            add(specialLabels(x,y+22,value.toString()));

        }
    }

    private void secStatsLabelCreator() {
        add(GUICharCreatorHelper.longLabelInitialization("SECONDARY STATISTICS", 15, 115));
        String secSkillName;
        Integer value;

        for (int i = 0, x = 15, y = 135; i < character.getSecStats().size(); i++, x+=27) {
            secSkillName = SecondaryStatsName.values()[i].getShortName();
            add(statsLabels(x,y,secSkillName));
            value = character.getSecStats().get(i).getSecondarySkillValue();
            add(statsLabels(x,y+22,value.toString()));
        }
    }

    private void skillsLabelCreator() {
        add(GUICharCreatorHelper.longLabelInitialization("SKILLS", 15, 185));
        String skillName;
        Integer value;
        Iterator<Map.Entry<String, Skills>> specialIterator = character.getSkills().entrySet().iterator();
        for (int i = 0, x = 15, y = 205; i < character.getSkills().size(); i++, x+=27) {
            skillName = SkillsName.values()[i].getSkillkey();
            add(statsLabels(x,y,skillName));
            Map.Entry<String, Skills> element = specialIterator.next();
            value = element.getValue().getSkillValue();
            add(statsLabels(x,y+22,value.toString()));

        }
    }

    private void itemsComponentsSetter() {
        //TODO podzielić na kilka metod

        add(GUICharCreatorHelper.longLabelInitialization("ACTIVE ITEMS", 15, 400));
        add(GUICharCreatorHelper.longLabelInitialization("ARMOR", 15, 430));
        JComboBox<String> armors = new JComboBox<>();
        for (int i = 0; i< ArmorsEnum.values().length; i++) {
            armors.addItem(ArmorsEnum.values()[i].getArmorName());
        }

        armors.setBounds(15, 450, 180, 20);
        armors.setFont(GUICharCreatorHelper.skillFont());
        add(armors);

        JLabel armorIcon = new JLabel();
        armorIcon.setOpaque(true);
        armorIcon.setBounds(15, 475, 180, 100);
        armorIcon.setBorder(GUICharCreatorHelper.borerToPane(""));
        armorIcon.setHorizontalAlignment(SwingConstants.CENTER);
        armorIcon.setVerticalAlignment(SwingConstants.CENTER);
        armorIcon.setIcon(new ImageIcon("rpgtoolgui/src/resources/FoTLeatherArmor.png"));
        add(armorIcon);

        add(GUICharCreatorHelper.longLabelInitialization("WEAPONS", 250, 430));

        JComboBox<String> weapons = new JComboBox<>();
        weapons.addItem("--NONE--");
        weapons.addItem("Knife");
        weapons.addItem("Plasma Rifle");
        weapons.setBounds(250, 450, 150, 20);
        weapons.setFont(GUICharCreatorHelper.skillFont());
        add(weapons);

        JComboBox<String> weapons1 = new JComboBox<>();
        weapons1.addItem("--NONE--");
        weapons1.addItem("Knife");
        weapons1.addItem("Plasma Rifle");
        weapons1.setBounds(410, 450, 150, 20);
        weapons1.setFont(GUICharCreatorHelper.skillFont());
        add(weapons1);

        JLabel primWeapon = new JLabel();
        primWeapon.setOpaque(true);
        primWeapon.setBounds(250, 475, 150, 100);
        primWeapon.setBorder(GUICharCreatorHelper.borerToPane(""));
        primWeapon.setHorizontalAlignment(SwingConstants.CENTER);
        primWeapon.setVerticalAlignment(SwingConstants.CENTER);
        primWeapon.setIcon(new ImageIcon("rpgtoolgui/src/resources/Fo1_combat_knife.png"));
        add(primWeapon);

        JLabel secWeapon = new JLabel();
        secWeapon.setOpaque(true);
        secWeapon.setBounds(410, 475, 150, 100);
        secWeapon.setBorder(GUICharCreatorHelper.borerToPane(""));
        secWeapon.setHorizontalAlignment(SwingConstants.CENTER);
        secWeapon.setVerticalAlignment(SwingConstants.CENTER);
        secWeapon.setIcon(new ImageIcon("rpgtoolgui/src/resources/Fo1_plasma_rifle.png"));
        add(secWeapon);
    }

    private void inventorySetter(){
        JButton openInventory = new JButton("Open Inventory");
        openInventory.setBounds(360,400,200,30);
        openInventory.setFont(GUICharCreatorHelper.skillFont());
        openInventory.addActionListener(invectoryListiner);
        openInventory.setActionCommand("invectory");
        this.add(openInventory);
    }

    private void traitsAndPerks(){
        this.add(GUICharCreatorHelper.longLabelInitialization("TRAITS",15,260));
        this.add(GUICharCreatorHelper.longLabelInitialization("PERKS",120,260));
        List<Trait> traits = character.getTraitsManager().getTraits();

        for (int i=0, y= 290; i< traits.size(); i++) {
            if(traits.get(i).isActiveTrait()){
                this.add(GUICharCreatorHelper.longLabelInitialization("- " + traits.get(i).getName(),15,y));
                y+=25;
            }
        }


    }

    private JLabel specialLabels(int x, int y, String text) {
        JLabel label = new JLabel(text);
        label.setBounds(x,y, 20,20);
        label.setOpaque(true);
        label.setBackground(new Color(255, 255, 255));
        label.setFont(GUICharCreatorHelper.skillFont());
        label.setHorizontalAlignment(SwingConstants.CENTER);
        return label;
    }

    private JLabel statsLabels(int x, int y, String text) {
        JLabel label = new JLabel(text);
        label.setBounds(x,y, 25,20);
        label.setOpaque(true);
        label.setFont(GUICharCreatorHelper.skillFont());
        label.setHorizontalAlignment(SwingConstants.CENTER);
        return label;
    }

    public JButton buttonInitializer(int x, int y, ActionListener listener) {
        JButton button = new JButton("ADD CHARACTER");
        button.setFont(GUICharCreatorHelper.windowFont());
        button.setBounds(x, y, 200, 45);
        button.addActionListener(listener);
        button.setActionCommand("dupa");

        return button;
    }


}
