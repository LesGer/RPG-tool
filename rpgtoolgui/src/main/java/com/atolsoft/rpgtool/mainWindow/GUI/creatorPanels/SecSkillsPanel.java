package com.atolsoft.rpgtool.mainWindow.GUI.creatorPanels;

import com.atolsoft.rpgtool.data.FalloutCharacter;
import com.atolsoft.rpgtool.mainWindow.GUI.GUICreatorHelpers.GUICharCreatorHelper;
import com.atolsoft.rpgtool.mainWindow.GUI.GUICreatorHelpers.GUILabelRefresher;

import javax.swing.*;
import java.awt.*;

import static com.atolsoft.rpgtool.mainWindow.GUI.GUICreatorHelpers.GUICharCreatorHelper.longLabelInitialization;

public class SecSkillsPanel extends JPanel {

    private FalloutCharacter character;
    private GUILabelRefresher labelRefresher;

    public SecSkillsPanel(FalloutCharacter character, GUILabelRefresher labelRefresher) {
        super();
        this.character = character;
        this.labelRefresher = labelRefresher;
        secondarySkillsPaneInitialization();
    }

    private void secondarySkillsPaneInitialization() {
        setLayout(null);
        setPreferredSize(new Dimension(350, 400));

        setBorder(GUICharCreatorHelper.borerToPane("Secondary Stats"));
        String skillName;
        System.out.println(character.getSecStats().size());
        for (int i = 0, y = 30; i < character.getSecStats().size(); i++, y += 25) {
            skillName = character.getSecStats().get(i).getSecondarySkillName();
            add(longLabelInitialization(skillName, 15, y));
            add(labelRefresher.getSecondaryStatsLabels()[i] = longLabelInitialization("", 230, y));
            System.out.println(skillName);
        }
    }
}
