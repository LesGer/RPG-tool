package com.atolsoft.rpgtool.mainWindow.GUI.creatorPanels;

import com.atolsoft.rpgtool.data.FalloutCharacter;
import com.atolsoft.rpgtool.mainWindow.GUI.GUICreatorHelpers.GUICharCreatorHelper;
import com.atolsoft.rpgtool.mainWindow.GUI.GUICreatorHelpers.GUILabelRefresher;
import com.atolsoft.rpgtool.util.traits.traits_specific.Trait;

import javax.swing.*;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.List;


public class HumanTraitsPanel extends JPanel implements ItemListener{

    private JCheckBox[] traitsButton;
    private int counter;
    private FalloutCharacter character;
    private GUILabelRefresher labelRefresher;


    public HumanTraitsPanel(FalloutCharacter fChar, GUILabelRefresher labelRefresher){
        super();
        this.character = fChar;
        this.labelRefresher = labelRefresher;
        traitsPaneInitialization();
    }

    private void traitsPaneInitialization() {
        traitsButton = new JCheckBox[thisTrait().size()];
        setBorder(GUICharCreatorHelper.borerToPane("Traits"));
        setLayout(null);

        for (int i = 0, x = 15, y = 30; i < thisTrait().size(); i++, y += 30) {
            if (y == 210 && x == 15) {
                x = 175;
                y = 30;
            }
            if (y == 210 && x == 175) {
                x = 325;
                y = 30;
            }
            add(traitsButton[i] = traitButtonInitializer(thisTrait().get(i).getName(), x, y));
        }

    }

    private JCheckBox traitButtonInitializer(String text, int x, int y) {
        JCheckBox tagSkill = new JCheckBox(text, false);
        tagSkill.setFont(GUICharCreatorHelper.skillFont());
        tagSkill.setBounds(x, y, 150, 25);
        tagSkill.addItemListener(this);

        return tagSkill;
    }

    private List<Trait> thisTrait() {
        return character.getTraitsManager().getTraits();
    }

    @Override
    public void itemStateChanged(ItemEvent e) {
        Object source = e.getItemSelectable();
        if (e.getStateChange() == ItemEvent.SELECTED) {
            for (int i=0; i< traitsButton.length; i++){
                if(source == traitsButton[i]){
                    thisTrait().get(i).addTrait(character);
                    thisTrait().get(i).setActiveTrait(true);
                    labelRefresher.setInfoTitleText(thisTrait().get(i).getName());
                    labelRefresher.setInfoText(thisTrait().get(i).printTraitInfo());
                    counter++;
                }
            }
            labelRefresher.refresh();
        }
        if (e.getStateChange() == ItemEvent.DESELECTED) {
            for (JCheckBox aSkillsButton : traitsButton) {
                if (source == aSkillsButton) {
                    System.out.println("działam i odejmuje");
                    counter--;
                }
            }
            labelRefresher.refresh();
        }
        GUICharCreatorHelper.checkBoxSetter(counter,2,traitsButton);
    }
}
