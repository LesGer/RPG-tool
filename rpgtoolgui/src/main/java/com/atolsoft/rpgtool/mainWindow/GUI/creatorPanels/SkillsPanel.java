package com.atolsoft.rpgtool.mainWindow.GUI.creatorPanels;

import com.atolsoft.rpgtool.data.FalloutCharacter;
import com.atolsoft.rpgtool.data.names.SkillsName;
import com.atolsoft.rpgtool.data.skills.Skills;
import com.atolsoft.rpgtool.mainWindow.GUI.GUICreatorHelpers.GUICharCreatorHelper;
import com.atolsoft.rpgtool.mainWindow.GUI.GUICreatorHelpers.GUILabelRefresher;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.Iterator;
import java.util.Map;

public class SkillsPanel extends JPanel implements ItemListener {

    private JCheckBox[] skillsButton;
    private FalloutCharacter character;
    private GUILabelRefresher labelRefresher;
    private int counter = 0;

    public SkillsPanel(FalloutCharacter character, GUILabelRefresher labelRefresher) {
        this.character = character;
        this.labelRefresher = labelRefresher;
        skillsButton = new JCheckBox[character.getSkills().size()];
        skillsPaneInitialization();
    }

    private void skillsPaneInitialization() {
        setPreferredSize(new Dimension(300, 200));
        setLayout(null);
        setBorder(GUICharCreatorHelper.borerToPane("Skills"));

        String skillName;

        for (int i = 0, y = 30; i < character.getSkills().size(); i++, y += 20) {
            skillName = SkillsName.values()[i].getSkillName();
            add(skillsButton[i] = skillButton(skillName, 15, y));
            add(labelRefresher.getSkillsLabels()[i] = shortLabelInitialization(null, 230, y));
        }
    }

    private JLabel shortLabelInitialization(String text, int x, int y) {
        JLabel label = new JLabel(text);
        label.setFont(GUICharCreatorHelper.skillFont());
        label.setBounds(x, y, 50, 25);
        return label;
    }

    private JCheckBox skillButton(String text, int x, int y) {
        JCheckBox tagSkill = new JCheckBox(text, false);
        tagSkill.setFont(GUICharCreatorHelper.skillFont());
        tagSkill.setBounds(x, y, 200, 25);
        tagSkill.addItemListener(this);

        return tagSkill;
    }

    public void itemStateChanged(ItemEvent e) {
        skillsButtonSetter(e);
        labelRefresher.refresh();
    }

    private void skillsButtonSetter(ItemEvent e) {
        Object source = e.getItemSelectable();
        if (e.getStateChange() == ItemEvent.SELECTED) {
            Iterator<Map.Entry<String, Skills>> skillsIterator = character.getSkills().entrySet().iterator();
            for (JCheckBox aSkillsButton : skillsButton) {
                Map.Entry<String, Skills> element = skillsIterator.next();
                if (source == aSkillsButton) {
                    element.getValue().setTagSkill(true);
                    counter++;
                }
            }
        }
        if (e.getStateChange() == ItemEvent.DESELECTED) {
            Iterator<Map.Entry<String, Skills>> skillsIterator = character.getSkills().entrySet().iterator();
            for (JCheckBox aSkillsButton : skillsButton) {
                Map.Entry<String, Skills> element = skillsIterator.next();
                if (source == aSkillsButton) {
                    element.getValue().setTagSkill(false);
                    counter--;
                }
            }
        }
        GUICharCreatorHelper.checkBoxSetter(counter,character.getTagSkillCounter(),skillsButton);
    }

    public int getCounter() {
        return counter;
    }
}
