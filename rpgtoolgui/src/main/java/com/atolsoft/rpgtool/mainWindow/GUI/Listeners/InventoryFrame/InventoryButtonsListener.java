package com.atolsoft.rpgtool.mainWindow.GUI.Listeners.InventoryFrame;

import com.atolsoft.rpgtool.data.Inventory.*;

import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class InventoryButtonsListener implements ActionListener {

    public static final String ADD_ITEM = "ADD_ITEM";
    public static final String ADD_ARMOR = "ADD_ARMOR";
    public static final String REMOVE = "REMOVE";
    public static final String OK = "OK";

    private InventoryList inventory;
    private Item itemToAdd;
    private ComboBoxItemListener inventoryListener;
    private DefaultTableModel model;
    private String itemName;

    public InventoryButtonsListener(InventoryList inventory, DefaultTableModel tableModel, ComboBoxItemListener listener) {
        this.inventoryListener = listener;
        this.model = tableModel;
        this.itemName = listener.getItemName();
        this.inventory = inventory;
    }




    @Override
    public void actionPerformed(ActionEvent e) {
        System.out.println(e.getActionCommand());

        switch (e.getActionCommand()) {
            case (ADD_ITEM):
                break;
            case (ADD_ARMOR):


                break;
            case (OK):
                System.out.println("zamknij okno");

                break;
        }

    }

    public InventoryList getInventory() {
        return inventory;
    }

    public void setInventory(InventoryList inventory) {
        this.inventory = inventory;
    }

    public Item getItemToAdd() {
        return itemToAdd;
    }

    public void setItemToAdd(Item itemToAdd) {
        this.itemToAdd = itemToAdd;
    }

//    private void addArmor() {
//        itemName = inventoryListener.getItemName();
//        System.out.println(itemName);
//        setArmourAsItem(itemName);
//        this.itemToAdd = setArmourAsItem(itemName);
//        this.inventory.getInventory().add(itemToAdd);
//    }

}
