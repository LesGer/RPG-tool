package com.atolsoft.rpgtool.mainWindow.GUI.creatorPanels;

import com.atolsoft.rpgtool.mainWindow.GUI.GUICreatorHelpers.GUICharCreatorHelper;
import com.atolsoft.rpgtool.mainWindow.GUI.GUICreatorHelpers.GUILabelRefresher;

import javax.swing.*;
import java.awt.*;

public class InfoPane extends JPanel {


    private GUILabelRefresher labelRefresher;
    private Font infoFont;

    public InfoPane(GUILabelRefresher labelRefresher){
        super();
        this.labelRefresher = labelRefresher;
        infoFont = new Font("Impact", Font.PLAIN, 13);
        infoPaneInitialization();
    }

    private void infoPaneInitialization(){
        setLayout(null);
        setBorder(GUICharCreatorHelper.borerToPane("Info"));

        labelRefresher.getInfoTitleLabel().setOpaque(true);
        labelRefresher.getInfoTitleLabel().setBounds(15, 30, 325, 25);
        labelRefresher.getInfoTitleLabel().setFont(GUICharCreatorHelper.windowFont());
        add(labelRefresher.getInfoTitleLabel());

        labelRefresher.getInfoLabel().setOpaque(true);
        labelRefresher.getInfoLabel().setFont(infoFont);
        labelRefresher.getInfoLabel().setBounds(15, 60, 325, 170);
        add(labelRefresher.getInfoLabel());

        JLabel imgLabel = new JLabel("Tytuł");
        imgLabel.setOpaque(true);
        imgLabel.setBackground(new Color(120, 6, 88));
        imgLabel.setBounds(345, 30, 150, 200);
        add(imgLabel);
    }
}
