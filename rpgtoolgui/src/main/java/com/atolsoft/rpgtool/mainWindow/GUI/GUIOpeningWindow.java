package com.atolsoft.rpgtool.mainWindow.GUI;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

public class GUIOpeningWindow implements ActionListener{

    private JFrame openMenuMainWindow;
    private JLabel topLabel;
    private JLabel pic;
    private JButton newCampain, loadCampain, exit;
    private JList<String> startList;

    public GUIOpeningWindow(){
        openMenuMainWindow = new JFrame("RPG tool - GUI - v0.001");
        windowInitializer();
        componentsInitializer();
        addLisiners();
    }

    private void windowInitializer(){
        openMenuMainWindow.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        openMenuMainWindow.setMinimumSize(new Dimension(800, 700));
        openMenuMainWindow.setMaximumSize(new Dimension(800, 700));
        openMenuMainWindow.setResizable(false);
        openMenuMainWindow.setLayout(null);
        openMenuMainWindow.pack();
        openMenuMainWindow.setVisible(true);
    }


    private void componentsInitializer() {
        topLabel = new JLabel("RPG tool - Fallout edition");
        pic = new JLabel(new ImageIcon("resources"+ File.separator+"mainMenuPic.png"));
        newCampain = new JButton("Nowa kampania");
        loadCampain = new JButton("Otwórz kampanię");
        exit = new JButton("Wyjście");
        startList= new JList<>();

        topLabel.setBounds       (260,20,300,60);
        pic.setBounds       (350,100,332,478);
        newCampain.setBounds   (80,100, 200, 100);
        loadCampain.setBounds  (80,220, 200, 100);
        startList.setBounds (80,340,200,100);
        exit.setBounds      (80,460, 200, 100);

        openMenuMainWindow.add(topLabel);
        openMenuMainWindow.add(newCampain);
        openMenuMainWindow.add(loadCampain);
        openMenuMainWindow.add(startList);
        openMenuMainWindow.add(exit);
        openMenuMainWindow.add(pic);
    }

    private void addLisiners(){
        newCampain.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        GUICampaignWindow campainWindow = new GUICampaignWindow();
        openMenuMainWindow.setVisible(false);
    }
}
