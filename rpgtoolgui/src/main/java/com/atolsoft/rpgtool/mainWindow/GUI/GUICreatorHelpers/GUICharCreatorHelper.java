package com.atolsoft.rpgtool.mainWindow.GUI.GUICreatorHelpers;


import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.awt.event.ActionListener;


public class GUICharCreatorHelper {

    private static Font windowFont = new Font("Impact", Font.PLAIN, 20);
    private static Font skillFont = new Font("Impact", Font.PLAIN, 15);

    public static Border borerToPane(String text) {
        Border black = BorderFactory.createLineBorder(new Color(1, 1, 1));
        return BorderFactory.createTitledBorder(black, text,
                TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.TOP, windowFont);
    }

    public static void checkBoxSetter(int counter, int maxCounter, JCheckBox[] button) {
        if (counter == maxCounter) {
            for (int i = 0; i < button.length; i++) {
                if (!button[i].isSelected()) {
                    button[i].setEnabled(false);
                }
            }
        }
        if (counter == maxCounter - 1) {
            for (int i = 0; i < button.length; i++) {
                if (!button[i].isSelected()) {
                    button[i].setEnabled(true);
                }
            }
        }
    }

    // FONTS

    public static Font windowFont() {
        return windowFont;
    }

    public static Font skillFont() {
        return skillFont;
    }

    // Labels, Buttons, Dialogs

    public static void ErrDialog(Component component, String text) {
        JOptionPane.showMessageDialog(component, text, "ERROR", JOptionPane.ERROR_MESSAGE);
    }

    public static JLabel longLabelInitialization(String text, int x, int y) {
        JLabel label = new JLabel(text);
        label.setFont(skillFont());
        label.setBounds(x, y, 200, 25);
        return label;
    }




}
