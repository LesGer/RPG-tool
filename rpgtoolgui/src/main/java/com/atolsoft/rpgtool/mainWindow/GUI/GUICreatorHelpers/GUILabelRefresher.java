package com.atolsoft.rpgtool.mainWindow.GUI.GUICreatorHelpers;

import com.atolsoft.rpgtool.data.CharacterDatabase;
import com.atolsoft.rpgtool.data.FalloutCharacter;
import com.atolsoft.rpgtool.data.Special;
import com.atolsoft.rpgtool.data.skills.Skills;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.util.Iterator;
import java.util.Map;

public class GUILabelRefresher {

    private FalloutCharacter character;
    private CharacterDatabase database;
    private JLabel[] secondaryStatsLabels;
    private JLabel[] specialLabels;
    private JLabel remSkillPointsLabel;
    private JLabel[] skillsLabels;
    private JLabel infoTitleLabel;
    private JLabel infoLabel;
    private String infoTitle, infoText;


    public JLabel[] getSecondaryStatsLabels() {
        return secondaryStatsLabels;
    }

    public void setSecondaryStatsLabels(JLabel[] secondaryStatsLabels) {
        this.secondaryStatsLabels = secondaryStatsLabels;
    }

    public JLabel[] getSpecialLabels() {
        return specialLabels;
    }

    public void setSpecialLabels(JLabel[] specialLabels) {
        this.specialLabels = specialLabels;
    }

    public JLabel getRemSkillPointsLabel() {
        return remSkillPointsLabel;
    }

    public void setRemSkillPointsLabel(JLabel remSkillPointsLabel) {
        this.remSkillPointsLabel = remSkillPointsLabel;
    }

    public JLabel[] getSkillsLabels() {
        return skillsLabels;
    }

    public void setSkillsLabels(JLabel[] skillsLabels) {
        this.skillsLabels = skillsLabels;
    }

    public JLabel getInfoTitleLabel() {
        return infoTitleLabel;
    }

    public void setInfoTitleLabel(JLabel infoTitleLabel) {
        this.infoTitleLabel = infoTitleLabel;
    }

    public JLabel getInfoLabel() {
        return infoLabel;
    }

    public void setInfoLabel(JLabel infoLabel) {
        this.infoLabel = infoLabel;
    }

    //------------------------------------------------------------------------------------------------------------------

    public GUILabelRefresher(FalloutCharacter character) {
        this.character = character;
        this.secondaryStatsLabels = new JLabel[character.getSecStats().size()];
        this.skillsLabels = new JLabel[character.getSkills().size()];
        this.remSkillPointsLabel = new JLabel();
        this.specialLabels = new JLabel[character.getSpecial().size()];
        this.infoTitleLabel = new JLabel();
        this.infoLabel = new JLabel();
    }

    public GUILabelRefresher(CharacterDatabase database) {
        this.database = database;
    }

    //------------------------------------------------------------------------------------------------------------------

    public void refresh(){
        refreshSecSkillsLabels();
        refreshSpecialLabels();
        refreshSkillsLabels();
        refrestInfo();
    }

    private void refreshSecSkillsLabels() {
        countSecondaryStats();
        Integer value;

        for (int i = 0; i < secondaryStatsLabels.length; i++) {
            value = character.getSecStats().get(i).getSecondarySkillValue();
            secondaryStatsLabels[i].setText(value.toString());
        }
    }

    private void refreshSpecialLabels() {
        Iterator<Map.Entry<String, Special>> specialIterator = character.getSpecial().entrySet().iterator();
        Integer value;
        for (JLabel specialLabel : specialLabels) {
            Map.Entry<String, Special> element = specialIterator.next();
            value = element.getValue().getSpecialValue();
            specialLabel.setText(value.toString());
        }
        value = character.getFreeSpecialPoints();
        remSkillPointsLabel.setText(String.format("Remaining points %d", value));
    }

    private void refreshSkillsLabels() {
        countSkills();
        Integer value;
        Iterator<Map.Entry<String, Skills>> skillsIterator = character.getSkills().entrySet().iterator();
        for (JLabel skills : skillsLabels) {
            Map.Entry<String, Skills> element = skillsIterator.next();
            value = element.getValue().getSkillValue();
            skills.setText(value.toString());
        }
    }

    private void refrestInfo(){
        infoTitleLabel.setText(infoTitle);
        infoLabel.setText(infoText);
    }

    public void setInfoTitleText(String title) {
        this.infoTitle = title;
    }

    public void setInfoText(String info) {
        this.infoText = info;
    }

    //----------------------- odswierzanie character creatora ----------------------------------------------------------
    //------------------------------------------------------------------------------------------------------------------

    private int thisSpecial(String key) {
        return character.getSpecial().get(key).getSpecialValue();
    }

    private void setThisSecStat(int index, int value) {
        character.getSecStats().get(index).setSecondarySkillValue(value);
    }

    private int countAP() {
        int ap = 5;
        if (thisSpecial("AG") == 1) {
            ap = 5;
        } else if (thisSpecial("AG") == 2 || thisSpecial("AG") == 3) {
            ap = 6;
        } else if (thisSpecial("AG") == 4 || thisSpecial("AG") == 5) {
            ap = 7;
        } else if (thisSpecial("AG") == 6 || thisSpecial("AG") == 7) {
            ap = 8;
        } else if (thisSpecial("AG") == 8 || thisSpecial("AG") == 9) {
            ap = 9;
        } else if (thisSpecial("AG") >= 10) {
            ap = 10;
        }
        return ap;
    }

    private int countMD() {
        int md = 0;
        if (thisSpecial("ST") <= 6) {
            md = 1;
        } else {
            md = (thisSpecial("ST") - 5);
        }
        return md;
    }

    private int countHR() {
        int hr = 0;
        if (thisSpecial("EN") <= 5) {
            hr = 1;
        } else if (thisSpecial("EN") <= 8 || thisSpecial("EN") > 5) {
            hr = 2;
        } else if (thisSpecial("EN") == 9 || thisSpecial("EN") == 10) {
            hr = 3;
        } else if (thisSpecial("EN") > 10) {
            hr = 4;
        }
        return hr;
    }

    private void countSecondaryStats() {
        setThisSecStat(0,15 + thisSpecial("ST") + (2* thisSpecial("EN")));
        setThisSecStat(1,thisSpecial("AG"));
        setThisSecStat(2,countAP());
        setThisSecStat(3,(int) (thisSpecial("ST") * 11.25));
        setThisSecStat(4,countMD());
        setThisSecStat(5,thisSpecial("EN") * 5);
        setThisSecStat(6,thisSpecial("EN")*2);
        setThisSecStat(10, thisSpecial("PE") * 2);
        setThisSecStat(11, countHR());
        setThisSecStat(12, thisSpecial("LK"));
    }

    private void countSkills() {
        for(Map.Entry<String,Skills> skills : character.getSkills().entrySet()) {
            skills.getValue().setCountedSkillValue(character);
        }
    }

    //------------------------------------------------------------------------------------------------------------------
}
