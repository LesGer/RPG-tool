package com.atolsoft.rpgtool.mainWindow.GUI.Listeners;

import com.atolsoft.rpgtool.data.CharacterDatabase;
import com.atolsoft.rpgtool.mainWindow.GUI.GUICharacterCreator;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AddCharacterListener implements ActionListener {

    private CharacterDatabase characterDatabase;

    public AddCharacterListener(CharacterDatabase characterDatabase) {
        this.characterDatabase = characterDatabase;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Thread thread = new Thread(() -> {
            new GUICharacterCreator(characterDatabase);
        });
        thread.start();
        try {
            thread.join();
        } catch (InterruptedException e1) {
            e1.printStackTrace();
        }
    }
}

