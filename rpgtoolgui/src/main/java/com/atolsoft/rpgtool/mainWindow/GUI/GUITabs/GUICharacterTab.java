package com.atolsoft.rpgtool.mainWindow.GUI.GUITabs;

import com.atolsoft.rpgtool.data.CharacterDatabase;
import com.atolsoft.rpgtool.mainWindow.GUI.Listeners.AddCharacterListener;

import javax.swing.*;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class GUICharacterTab extends JPanel {

    private CharacterDatabase characterDatabase;
    private AddCharacterListener listener;
    private List<JPanel> characterPanel;


    public GUICharacterTab(CharacterDatabase characterDatabase) {
        super();
        this.characterDatabase = characterDatabase;
        this.listener = new AddCharacterListener(characterDatabase);
        this.characterPanel = new ArrayList<>();
        this.setLayout(new GridLayout(0, 2, 10, 10));
        addCharacterToPanel();
        Thread refresher = refresher();
        refresher.start();
    }

    //----------------------------TOP PANEL ---------------------------------------------------

    private void addCharacterToPanel() {
        if (characterDatabase.getCharacterArray().size() == 0) {
            System.out.println();
            characterPanel.add(new CampaignCharacterPanel(listener));
            this.add(characterPanel.get(0));
        } else {
            for (int i = 0; i < characterDatabase.getCharacterArray().size(); i++) {
                characterPanel.add(new CampaignCharacterPanel(characterDatabase.getCharacterArray().get(i)));
                this.add(characterPanel.get(i));
            }
        }
    }

    private Thread refresher() {
        return new Thread(() -> {
            while (true) {
                synchronized (characterDatabase) {
                    try {
                        characterDatabase.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    refreshPanels();
                }
            }
        });
    }


    private void refreshPanels() {
        for (JPanel jPanel : characterPanel) {
            this.remove(jPanel);
            this.revalidate();
            this.repaint();
        }
        characterPanel.clear();
        this.revalidate();
        this.repaint();
        addCharacterToPanel();
    }

}
