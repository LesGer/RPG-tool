package com.atolsoft.rpgtool.mainWindow.GUI.creatorPanels;

import com.atolsoft.rpgtool.data.FalloutCharacter;
import com.atolsoft.rpgtool.data.Special;
import com.atolsoft.rpgtool.data.names.SpecialNames;
import com.atolsoft.rpgtool.mainWindow.GUI.GUICreatorHelpers.GUICharCreatorHelper;
import com.atolsoft.rpgtool.mainWindow.GUI.GUICreatorHelpers.GUILabelRefresher;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SpecialPanel extends JPanel implements ActionListener {

    private FalloutCharacter character;
    private GUILabelRefresher labelRefresher;

    public SpecialPanel(FalloutCharacter fChar, GUILabelRefresher labelRefresher) {
        super();
        this.character = fChar;
        this.labelRefresher = labelRefresher;
        specialModificationPaneInitialization();
    }

    private void specialModificationPaneInitialization() {
        setPreferredSize(new Dimension(300, 400));
        setLayout(null);
        setBorder(GUICharCreatorHelper.borerToPane("Add Special Points"));
        String specialName;
        for (int i = 0, j = 0, y = 40, x = 15; i < character.getSpecial().size(); i++, j += 2, y += 47) {
            specialName = SpecialNames.values()[i].getSpecialKey();
            add(specialLabelInitialization(specialName, x, y));
            add(buttonInitializer("+", x += 45, y, j));
            add(labelRefresher.getSpecialLabels()[i] = specialLabelInitialization("", x += 45, y));
            add(buttonInitializer("-", x += 45, y, j + 1));
            x = 15;
        }
        Integer value = character.getFreeSpecialPoints();
        labelRefresher.setRemSkillPointsLabel(GUICharCreatorHelper.longLabelInitialization(
                String.format("Remaining points %d", value), 60, 375));
        add(labelRefresher.getRemSkillPointsLabel() );

    }

    private JButton buttonInitializer(String text, int x, int y, int lisiner) {
        JButton button = new JButton(text);
        button.setFont(GUICharCreatorHelper.windowFont());
        button.setBounds(x, y, 45, 45);
        button.addActionListener(this);
        button.setActionCommand(String.valueOf(lisiner));

        return button;
    }

    private JLabel specialLabelInitialization(String text, int x, int y) {
        JLabel label = new JLabel(text);
        label.setFont(GUICharCreatorHelper.windowFont());
        label.setHorizontalAlignment(SwingConstants.CENTER);
        label.setBounds(x, y, 45, 45);
        return label;
    }

    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {
            case "0": addSpecial("ST"); break;
            case "1": remSpecial("ST"); break;
            case "2": addSpecial("PE"); break;
            case "3": remSpecial("PE"); break;
            case "4": addSpecial("EN"); break;
            case "5": remSpecial("EN"); break;
            case "6": addSpecial("CH"); break;
            case "7": remSpecial("CH"); break;
            case "8": addSpecial("IN"); break;
            case "9": remSpecial("IN"); break;
            case "10": addSpecial("AG"); break;
            case "11": remSpecial("AG"); break;
            case "12": addSpecial("LK"); break;
            case "13": remSpecial("LK"); break;
        }
    }

    private void addSpecial(String key) {
        Special thisSpecial = character.getSpecial().get(key);
        if (thisSpecial.getSpecialValue() < thisSpecial.getSpecialMaxValue()
                && character.getFreeSpecialPoints() != 0) {
            thisSpecial.setSpecialValue(thisSpecial.getSpecialValue() + 1);
            System.out.println("Dodano punkt ");
            character.setFreeSpecialPoints(character.getFreeSpecialPoints() - 1);
        } else {
            System.out.println("nie można dodać punktu");
        }
        labelRefresher.refresh();
    }

    private void remSpecial(String key) {
        Special thisSpecial = character.getSpecial().get(key);
        if (thisSpecial.getSpecialValue() > thisSpecial.getSpecialMinValue()) {
            thisSpecial.setSpecialValue(thisSpecial.getSpecialValue() - 1);
            System.out.println("Dodano punkt ");
            character.setFreeSpecialPoints(character.getFreeSpecialPoints() + 1);
        } else {
            System.out.println("nie można odjąć punktu siły");
        }
        labelRefresher.refresh();
    }

}
