package com.atolsoft.rpgtool.mainWindow.GUI.Listeners;

import com.atolsoft.rpgtool.mainWindow.GUI.GUITabs.InvectoryWindow.GUIInventoryFrame;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class InventoryListener implements ActionListener{

    @Override
    public void actionPerformed(ActionEvent e) {
        e.getActionCommand();
        //TODO otwiera okienko z invectory
        Thread thread = new Thread(() -> new GUIInventoryFrame());
        thread.start();
    }
}
