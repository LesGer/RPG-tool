package com.atolsoft.rpgtool.mainWindow.GUI;


import com.atolsoft.rpgtool.data.CharacterDatabase;
import com.atolsoft.rpgtool.data.FalloutCharacter;
import com.atolsoft.rpgtool.data.Human;
import com.atolsoft.rpgtool.mainWindow.GUI.GUICreatorHelpers.GUICharCreatorHelper;
import com.atolsoft.rpgtool.mainWindow.GUI.GUICreatorHelpers.GUILabelRefresher;
import com.atolsoft.rpgtool.mainWindow.GUI.creatorPanels.*;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class GUICharacterCreator implements ActionListener {
    private FalloutCharacter fChar;
    private JFrame mainWindow;
    private JComboBox<String> raceChooser;
    private CharacterDatabase characterDatabase;
    private JTextField nameLabel, ageLabel;
    private SkillsPanel skillsPanel;


    public GUICharacterCreator(CharacterDatabase characterDatabase) {
        this.characterDatabase = characterDatabase;
        fChar = new Human();
        mainWindow = new JFrame();
        windowInitializer();

    }

    private void topPaneInitializer() {
        JPanel topPane = new JPanel();
        topPane.setLayout(new BoxLayout(topPane, BoxLayout.LINE_AXIS));
        topPane.add(topLabelInitialization("Name: "));
        topPane.add(nameLabel = topTextFieldInitialization(10));
        topPane.add(Box.createRigidArea(new Dimension(10, 0)));
        topPane.add(topLabelInitialization("Age: "));
        topPane.add(ageLabel = topTextFieldInitialization(5));
        topPane.add(Box.createRigidArea(new Dimension(10, 0)));
        topPane.add(topLabelInitialization("Select race: "));
        topPane.add(raceChooser = topComboBoxInitialization());
        topPane.add(Box.createRigidArea(new Dimension(100, 0)));
        topPane.add(topPaneButton("RESET"));
        topPane.add(topPaneButton("RANDOM"));
        topPane.add(topPaneButton("OK"));
        mainWindow.getContentPane().add(topPane, BorderLayout.PAGE_START);
    }


    private JComboBox<String> topComboBoxInitialization() {
        JComboBox<String> raceCombo = new JComboBox<>();
        raceCombo.setFont(GUICharCreatorHelper.windowFont());
        raceCombo.addItem("Human");
        raceCombo.addItem("Ghul");
        return raceCombo;
    }

    private JButton topPaneButton(String text) {
        JButton button = new JButton(text);
        button.addActionListener(this);
        button.setActionCommand(text);
        button.setFont(GUICharCreatorHelper.windowFont());
        return button;
    }


    private void windowInitializer() {
        mainWindow.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        mainWindow.setMinimumSize(new Dimension(1024, 768));
        mainWindow.setPreferredSize(new Dimension(1024, 768));  //XGA
        mainWindow.getContentPane().add(mainPaneInitialization(), BorderLayout.CENTER);
        topPaneInitializer();
        mainWindow.pack();
        mainWindow.setVisible(true);
    }

    private JPanel mainPaneInitialization() {
        JPanel mainPane = new JPanel();
        mainPane.setLayout(new BoxLayout(mainPane, BoxLayout.PAGE_AXIS));

        JPanel topPane = new JPanel();
        topPane.setPreferredSize(new Dimension(400, 520));
        topPane.setLayout(new GridLayout(0, 3));


        JPanel bottomPane = new JPanel();
        bottomPane.setPreferredSize(new Dimension(400, 280));
        bottomPane.setLayout(new GridLayout(0, 2));


        GUILabelRefresher guiLabelRefresher = new GUILabelRefresher(fChar);
        SpecialPanel specialPanel = new SpecialPanel(fChar, guiLabelRefresher);
        topPane.add(specialPanel);

        topPane.add(new SecSkillsPanel(fChar, guiLabelRefresher));
        skillsPanel = new SkillsPanel(fChar, guiLabelRefresher);
        topPane.add(skillsPanel);

        JPanel bottomRight = new JPanel();
        bottomRight.setBorder(GUICharCreatorHelper.borerToPane("Right"));
        JButton ok = new JButton("OK");
        JEditorPane textField = new JEditorPane();
        bottomRight.add(textField);
        bottomRight.add(ok);


        HumanTraitsPanel humanTraitsPanel = new HumanTraitsPanel(fChar, guiLabelRefresher);
        bottomPane.add(humanTraitsPanel);
        bottomPane.add(new InfoPane(guiLabelRefresher));

        mainPane.add(topPane);
        mainPane.add(bottomPane);
        guiLabelRefresher.refresh();
        return mainPane;
    }

    private JLabel topLabelInitialization(String text) {
        JLabel label = new JLabel(text);
        label.setFont(GUICharCreatorHelper.windowFont());
        return label;
    }

    private JTextField topTextFieldInitialization(int width) {
        JTextField textField = new JTextField();
        textField.setFont(GUICharCreatorHelper.windowFont());
        textField.setColumns(width);
        return textField;
    }

    private FalloutCharacter createCharacter() {
        if (raceChooser.getSelectedItem().equals("Human")) {
            fChar.setName(nameLabel.getText());
            Integer age = Integer.parseInt(ageLabel.getText());
            fChar.setAge(age);
            fChar.setLevel(1);
            fChar.setXp(0);
        }
        return fChar;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        System.out.println(e.getActionCommand());

        if (nameLabel.getText().equals("")) {
            GUICharCreatorHelper.ErrDialog(mainWindow, "ENTER NAME");
        } else if (ageLabel.getText().equals("")) {
            GUICharCreatorHelper.ErrDialog(mainWindow, "ENTER AGE");
        } else if (fChar.getFreeSpecialPoints() != 0) {
            GUICharCreatorHelper.ErrDialog(mainWindow, "YOU MUST SPEND ALL FREE SPECIAL POINTS");
        } else if (fChar.getTagSkillCounter() != skillsPanel.getCounter()) {
            GUICharCreatorHelper.ErrDialog(mainWindow, "YOU MUST SET ALL TAG SKILLS");
        } else {

            characterDatabase.addCharacter(createCharacter());
            characterDatabase.printCharacterList();

            synchronized (characterDatabase) {
                characterDatabase.notify();
            }

            mainWindow.setVisible(false);
            mainWindow.dispose();
        }


    }
}