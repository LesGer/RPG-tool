Toolbox desighned for Fallout RPG system players and DM's.

Changelog:

v 0.1 - Character creator - console
Features:
- Character creator (only Human) - console

v 0.2 - Character creator - swing
- Character creator (only Human) - window
    * character traits
    * tag skills
- Campain window:
    * Character display

v 0.2.5 - Inventory window - swing

