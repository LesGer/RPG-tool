package com.atolsoft.rpgtool.util.traits.traits_specific;


import com.atolsoft.rpgtool.data.FalloutCharacter;
import com.atolsoft.rpgtool.data.skills.Science;

public class BetaSoftware extends Trait {


    @Override
    public String getName() {
        return "Beta Software";
    }

    @Override
    public void addTrait(FalloutCharacter fChar) {

    }

    @Override
    public void remTrait(FalloutCharacter fChar) {

    }

    @Override
    public String printTraitInfo() {
        return "<html>You have been equipped with an experimental operating system and peripherals. " +
                "You get 1 extra Tag Skill, but whenever using ANY tag skill, you must roll against Luck or suffer an " +
                "of all APs for that round.</html>";
    }


}
