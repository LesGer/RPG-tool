package com.atolsoft.rpgtool.util.traits.traits_specific;


import com.atolsoft.rpgtool.data.FalloutCharacter;

public class VatSkin extends Trait {
    @Override
    public String getName() {
        return "Vat skin";
    }

    @Override
    public void addTrait(FalloutCharacter fChar) {
        fChar.getSecStats().get(1).setSecondarySkillValue(fChar.getSecStats().get(1).getSecondarySkillValue()+10);
        // -1 Percepcji dla wszystkich w obrębie 10 hexów
    }

    @Override
    public void remTrait(FalloutCharacter fChar) {
        fChar.getSecStats().get(1).setSecondarySkillValue(fChar.getSecStats().get(1).getSecondarySkillValue()-10);
        // -1 Percepcji dla wszystkich w obrębie 10 hexów
    }

    @Override
    public String printTraitInfo() {
        return "<html>Other people find you hideous to behold and disgusting to smell after your “dip” in the FEV vats. " +
                "The good news is that you gain a +10 bonus to your Armor Class thanks to your extra-tough skin. " +
                "The bad news is that everyone within ten hexes of your location, friend and foe, suffers a 1-point " +
                "penalty to Perception (you are unaffected). Only Mutants can choose this trait.</html>";
    }
}
