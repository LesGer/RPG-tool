package com.atolsoft.rpgtool.util.traits;


import com.atolsoft.rpgtool.data.FalloutCharacter;
import com.atolsoft.rpgtool.util.traits.traits_specific.Trait;

import java.util.List;


public abstract class TraitsManager {

    public static final int TRAIT_COUNT = 2;
    private List<Trait> traits;
    private int traitsCount = 0;

    public List<Trait> getTraits() {
        return traits;
    }

    public void setTraits(List<Trait> traits) {
        this.traits = traits;
    }

    public int getTraitsCount() {
        return traitsCount;
    }

    public void setTraitsCount(int traitsCount) {
        this.traitsCount = traitsCount;
    }


    private void addTraits(int index, FalloutCharacter fChar){
        traits.get(index).setActiveTrait(true);
        traits.get(index).addTrait(fChar);
        traitsCount++;
    }

    private void remTraits(int index, FalloutCharacter fChar){
        traits.get(index).setActiveTrait(false);
        traits.get(index).remTrait(fChar);
        traitsCount--;
    }

    private String chceckTrait(int i) {
        String active = (traits.get(i).isActiveTrait()) ? "X" : " ";
        return active;
    }

}
