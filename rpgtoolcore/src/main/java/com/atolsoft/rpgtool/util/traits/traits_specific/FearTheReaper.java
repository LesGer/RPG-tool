package com.atolsoft.rpgtool.util.traits.traits_specific;


import com.atolsoft.rpgtool.data.FalloutCharacter;

public class FearTheReaper extends Trait {

    @Override
    public String getName() {
        return "Fear the Reaper";
    }

    @Override
    public void addTrait(FalloutCharacter fChar) {
        // to do
    }

    @Override
    public void remTrait(FalloutCharacter fChar) {
        // to do
    }

    @Override
    public String printTraitInfo() {
        return "<html>You have cheated death! You gain perks as if you were a human, but you are now on death’s short " +
                "list. This means that once a month, you must roll against Luck or else drop dead. " +
                "Only Ghouls can choose this trait.</html>";
    }
}
