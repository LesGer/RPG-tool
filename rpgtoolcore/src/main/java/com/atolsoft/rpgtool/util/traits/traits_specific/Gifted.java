package com.atolsoft.rpgtool.util.traits.traits_specific;


import com.atolsoft.rpgtool.data.FalloutCharacter;
import com.atolsoft.rpgtool.data.Special;

import java.util.Iterator;
import java.util.Map;

public class Gifted extends Trait {

    @Override
    public String getName() {
        return "Gifted";
    }

    @Override
    public void addTrait(FalloutCharacter fChar) {
        Iterator<Map.Entry<String,Special>> iterator = fChar.getSpecial().entrySet().iterator();
        Iterator<Map.Entry<String,Special>> iterator1 = fChar.getSpecial().entrySet().iterator();
        for(int i=0; i<fChar.getSpecial().size() || iterator.hasNext(); i++){
            iterator.next().getValue().setSpecialValue(iterator1.next().getValue().getSpecialValue()+1);
        }
        for (int i = 0; i < fChar.getSecStats().size(); i++) {
            fChar.getSecStats().get(i).setSecondarySkillValue(fChar.getSecStats().get(i).getSecondarySkillValue() - 10);
        }
    }

    @Override
    public void remTrait(FalloutCharacter fChar) {
        Iterator<Map.Entry<String,Special>> iterator = fChar.getSpecial().entrySet().iterator();
        Iterator<Map.Entry<String,Special>> iterator1 = fChar.getSpecial().entrySet().iterator();
        for(int i=0; i<fChar.getSpecial().size() || iterator.hasNext(); i++){
            iterator.next().getValue().setSpecialValue(iterator1.next().getValue().getSpecialValue()-1);
        }
        for (int i = 0; i < fChar.getSecStats().size(); i++) {
            fChar.getSecStats().get(i).setSecondarySkillValue(fChar.getSecStats().get(i).getSecondarySkillValue() + 10);
        }


    }

    @Override
    public String printTraitInfo() {
        return "<html>You have more innate abilities than most, so you have not spent as much time honing your skills. " +
                "Your statistics are better than the average person, but your skills are lacking. All Stats get " +
                "a 1- point bonus, but all skills get a 10% penalty and you receive 5 less Skill Points per level.</html>";
    }
}
