package com.atolsoft.rpgtool.util.traits.traits_specific;


import com.atolsoft.rpgtool.data.FalloutCharacter;

public class TargetingComputer extends Trait {

    @Override
    public String getName() {
        return "Targeting computer";
    }

    @Override
    public void addTrait(FalloutCharacter fChar) {

    }

    @Override
    public void remTrait(FalloutCharacter fChar) {

    }

    @Override
    public String printTraitInfo() {
        return "<html>You have been programmed with an onboard targeting computer. All attacks cost 1 extra AP to perform, " +
                "but you can always add +15% to your chance to-hit.</html>";
    }
}
