package com.atolsoft.rpgtool.util.traits.traits_specific;


import com.atolsoft.rpgtool.data.FalloutCharacter;

public class TechWizzard extends Trait {

    @Override
    public String getName() {
        return "Tech Wizzard";
    }

    @Override
    public void addTrait(FalloutCharacter fChar) {
        fChar.getSpecial().get("PE").setSpecialValue(fChar.getSpecial().get("PE").getSpecialValue() - 1);
        fChar.getSkills().get(9).setSkillValue(fChar.getSkills().get(9).getSkillValue() + 15);
        fChar.getSkills().get(12).setSkillValue(fChar.getSkills().get(12).getSkillValue() + 15);
        fChar.getSkills().get(12).setSkillValue(fChar.getSkills().get(12).getSkillValue() + 15);
    }

    @Override
    public void remTrait(FalloutCharacter fChar) {
        fChar.getSpecial().get("PE").setSpecialValue(fChar.getSpecial().get("PE").getSpecialValue() + 11);
        fChar.getSkills().get(9).setSkillValue(fChar.getSkills().get(9).getSkillValue() - 15);
        fChar.getSkills().get(12).setSkillValue(fChar.getSkills().get(12).getSkillValue() - 15);
        fChar.getSkills().get(12).setSkillValue(fChar.getSkills().get(12).getSkillValue() - 15);
    }

    @Override
    public String printTraitInfo() {
        return "<html>You spent your formative years hunched over a bench learning all about the way things work. " +
                "The trouble is that you’ve ruined your eyes! You get a +15% bonus to Science, Repair, and Lockpick" +
                " skills, but you lose 1 Perception.</html>";
    }
}
