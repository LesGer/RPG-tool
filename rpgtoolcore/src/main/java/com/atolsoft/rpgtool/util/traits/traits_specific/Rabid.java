package com.atolsoft.rpgtool.util.traits.traits_specific;


import com.atolsoft.rpgtool.data.FalloutCharacter;

public class Rabid extends Trait {

    @Override
    public String getName() {
        return "Rabid";
    }

    @Override
    public void addTrait(FalloutCharacter fChar) {

    }

    @Override
    public void remTrait(FalloutCharacter fChar) {

    }

    @Override
    public String printTraitInfo() {
        return "<html>You are a half-crazed, feral killing machine. You are not affected by crippled limbs (blindness " +
                "still affects you normally), and every time you kill an opponent in combat, you get 5 more APs " +
                "that round. Chems, including stimpaks, have no effect on you.</html>";
    }
}
