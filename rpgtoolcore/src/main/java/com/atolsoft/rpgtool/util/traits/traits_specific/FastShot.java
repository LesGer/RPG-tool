package com.atolsoft.rpgtool.util.traits.traits_specific;


import com.atolsoft.rpgtool.data.FalloutCharacter;

public class FastShot extends Trait {

    @Override
    public String getName() {
        return "Fast Shot";
    }

    @Override
    public void addTrait(FalloutCharacter fChar) {
            System.out.println("Nie dla psa kiełbasa");
        // -1AP za strzeł
        // brak możliwości celowania
    }

    @Override
    public void remTrait(FalloutCharacter fChar) {
            System.out.println("Nie dla psa kiełbasa");
        // -1AP za strzeł
        // brak możliwości celowania
    }

    @Override
    public String printTraitInfo() {
        return "<html>You don't have time for a targeted attack, because you attack faster than normal people. " +
                "It costs you one less action point to use a weapon. You cannot perform targeted shots, but all " +
                "weapons take one less action point to use. Note that the Fast Shot trait has no effect on HtH or " +
                "Melee attacks.</html>";
    }
}
