package com.atolsoft.rpgtool.util.traits.traits_specific;


import com.atolsoft.rpgtool.data.FalloutCharacter;

public class FastMetabolism extends Trait {

    @Override
    public String getName() {
        return "Fast Metabolism";
    }

    @Override
    public void addTrait(FalloutCharacter fChar) {
        fChar.getSecStats().get(12).setSecondarySkillValue(fChar.getSecStats().get(12).getSecondarySkillValue()*2);
        fChar.getSecStats().get(5).setSecondarySkillValue(0);
        fChar.getSecStats().get(6).setSecondarySkillValue(0);

    }

    @Override
    public void remTrait(FalloutCharacter fChar) {
        fChar.getSecStats().get(12).setSecondarySkillValue(fChar.getSecStats().get(12).getSecondarySkillValue()/2);
       // fChar.getSecStats().get(5).setSecondarySkillValue(new CharacterCreator().countPR(fChar));
        //fChar.getSecStats().get(6).setSecondarySkillValue(new CharacterCreator().countRR(fChar));
    }

    @Override
    public String printTraitInfo() {
        return "<html>Your metabolic rate is twice normal. This means that you are much less resistant to radiation " +
                "and poison, but your body heals faster. You get a 2 point bonus to Healing Rate, but your Radiation " +
                "and Poison Resistance start at 0%.</html>";
    }
}
