package com.atolsoft.rpgtool.util.traits.traits_specific;


import com.atolsoft.rpgtool.data.FalloutCharacter;

public class TightNuts extends Trait {

    @Override
    public String getName() {
        return "Tight nuts";
    }

    @Override
    public void addTrait(FalloutCharacter fChar) {

    }

    @Override
    public void remTrait(FalloutCharacter fChar) {

    }

    @Override
    public String printTraitInfo() {
        return "<html>This robot was built to take the knocks. You get double the base Damage Resistance to any attack, " +
                "but you gain only half the Hit Points back from repairs.</html>";
    }
}
