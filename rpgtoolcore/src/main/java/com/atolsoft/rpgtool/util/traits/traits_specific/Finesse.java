package com.atolsoft.rpgtool.util.traits.traits_specific;


import com.atolsoft.rpgtool.data.FalloutCharacter;

public class Finesse extends Trait {

    @Override
    public String getName() {
        return "Finesse";
    }

    @Override
    public void addTrait(FalloutCharacter fChar) {
        fChar.getSecStats().get(12).setSecondarySkillValue(fChar.getSecStats().get(12).getSecondarySkillValue()+10);
        //dmg - 30%
    }

    @Override
    public void remTrait(FalloutCharacter fChar) {
        fChar.getSecStats().get(12).setSecondarySkillValue(fChar.getSecStats().get(12).getSecondarySkillValue()-10);
        //dmg + 30%
    }

    @Override
    public String printTraitInfo() {
        return "<html>Your attacks show a lot of finesse. You don't do as much damage, but you cause more critical hits. " +
                "All of your attacks lose 30% of their damage (after reductions are made for Damage Resistance, etc.) " +
                "but you gain a 10% bonus to Critical Chance.</html>";
    }
}
