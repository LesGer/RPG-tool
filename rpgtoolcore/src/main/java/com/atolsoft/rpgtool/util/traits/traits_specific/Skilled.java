package com.atolsoft.rpgtool.util.traits.traits_specific;


import com.atolsoft.rpgtool.data.FalloutCharacter;
import com.atolsoft.rpgtool.data.skills.Skills;

import java.util.Iterator;
import java.util.Map;

public class Skilled extends Trait {

    @Override
    public String getName() {
        return "Skilled";
    }

    @Override
    public void addTrait(FalloutCharacter fChar) {
        System.out.println(isActiveTrait());
        setActiveTrait(true);
        System.out.println(isActiveTrait());
    }

    @Override
    public void remTrait(FalloutCharacter fChar) {
        setActiveTrait(false);
        //zmienić liczbę perków
    }

    @Override
    public String printTraitInfo() {
        return "<html>Since you spend more time improving your skills than a normal person, you gain more skill points. " +
                "The tradeoff is that you do not gain as many extra abilities. You will gain a perk at one level " +
                "higher than normal. For example, if you normally gained a perk every 4 levels, you would now gain " +
                "a perk every 5 levels. You will get an additional 5 skill points per new experience level, and a " +
                "one-time bonus of +10% to your skills when you begin the game.</html>";
    }
}
