package com.atolsoft.rpgtool.util.traits.traits_specific;


import com.atolsoft.rpgtool.data.FalloutCharacter;

public abstract class Trait {

    private boolean activeTrait;

    public Trait(){
        this.activeTrait = false;
    }

    public boolean isActiveTrait() {
        return activeTrait;
    }

    public void setActiveTrait(boolean activeTrait) {
        this.activeTrait = activeTrait;
    }

    public abstract String getName();

    public abstract void addTrait(FalloutCharacter fChar);

    public abstract void remTrait(FalloutCharacter fChar);

    public abstract String printTraitInfo();


}
