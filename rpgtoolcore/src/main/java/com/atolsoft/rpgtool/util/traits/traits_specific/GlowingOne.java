package com.atolsoft.rpgtool.util.traits.traits_specific;


import com.atolsoft.rpgtool.data.FalloutCharacter;

public class GlowingOne extends Trait {
    @Override
    public String getName() {
        return "Glowing one";
    }

    @Override
    public void addTrait(FalloutCharacter fChar) {
        fChar.getSecStats().get(6).setSecondarySkillValue(fChar.getSecStats().get(6).getSecondarySkillValue()+50);
    }

    @Override
    public void remTrait(FalloutCharacter fChar) {
        fChar.getSecStats().get(6).setSecondarySkillValue(fChar.getSecStats().get(6).getSecondarySkillValue()-50);
    }

    @Override
    public String printTraitInfo() {
        return "<html>Extreme radiation exposure has left you glowing in the dark. Your glow eliminates modifiers from " +
                "light in combat for both you and your enemies. In addition, you gain a +50% bonus to " +
                "Radiation Resistance, but everyone around you takes 10 rads per hour</html>";
    }
}
