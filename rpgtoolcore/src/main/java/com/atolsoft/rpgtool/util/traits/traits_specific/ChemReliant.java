package com.atolsoft.rpgtool.util.traits.traits_specific;


import com.atolsoft.rpgtool.data.FalloutCharacter;

public class ChemReliant extends Trait {

    @Override
    public String getName() {
        return "Chem Reliant";
    }

    @Override
    public void addTrait(FalloutCharacter fChar) {
        System.out.println("Chem reliant");
    }

    @Override
    public void remTrait(FalloutCharacter fChar) {
        System.out.println("Chem reliant");
    }

    @Override
    public String printTraitInfo() {
        return "<html>You are more easily addicted to chems. Your chance to be addicted is twice " +
                "normal, but you recover in half the time from their ill effects. Robots cannot choose this trait.</html>";
    }
}
