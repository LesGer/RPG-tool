package com.atolsoft.rpgtool.util.traits.traits_specific;


import com.atolsoft.rpgtool.data.FalloutCharacter;

public class Jinxed extends Trait {

    @Override
    public String getName() {
        return "Jinxed";
    }

    @Override
    public void addTrait(FalloutCharacter fChar) {
        System.out.println("Jinxed!");
    }

    @Override
    public void remTrait(FalloutCharacter fChar) {
        System.out.println("Jinxed!");
    }

    @Override
    public String printTraitInfo() {
        return "<html>The good thing is that everyone around you has more critical failures in combat. The bad thing is: " +
                "so do you! If you, a member of your party, or a non-player character have a failure in combat, " +
                "there is a greater likelihood the failure will be upgraded (downgraded?) to a critical failure. " +
                "Critical failures are bad: weapons explode, you may hit the wrong target, you could lose part of " +
                "your turn, or any number of bad things. Failures are 50% more likely to become critical failures " +
                "around the character or anyone else in combat.</html>";
    }
}
