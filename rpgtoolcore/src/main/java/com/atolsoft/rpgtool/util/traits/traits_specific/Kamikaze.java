package com.atolsoft.rpgtool.util.traits.traits_specific;


import com.atolsoft.rpgtool.data.FalloutCharacter;

public class Kamikaze extends Trait {

    @Override
    public String getName() {
        return "Kamikaze";
    }

    @Override
    public void addTrait(FalloutCharacter fChar) {
        fChar.getSecStats().get(1).setSecondarySkillValue(0);
        fChar.getSecStats().get(12).setSecondarySkillValue(fChar.getSecStats().get(12).getSecondarySkillValue()+5);
    }

    @Override
    public void remTrait(FalloutCharacter fChar) {
        //fChar.getSecStats().get(1).setSecondarySkillValue(new CharacterCreator().countAC(fChar));
        fChar.getSecStats().get(12).setSecondarySkillValue(fChar.getSecStats().get(12).getSecondarySkillValue()-5);
    }

    @Override
    public String printTraitInfo() {
        return "<html>By not paying attention to any threats, you can act a lot faster in a turn. This lowers your " +
                "Armor Class to just what you are wearing, but you sequence much faster in a combat turn. You have " +
                "no natural Armor Class (Armor Class is therefore 0 regardless of Agility). You must wear armor to " +
                "get an Armor Class. Your sequence gets a 5 point bonus.</html>";
    }
}
