package com.atolsoft.rpgtool.util.traits.traits_specific;


import com.atolsoft.rpgtool.data.FalloutCharacter;

public class NightPerson extends Trait {
    @Override
    public String getName() {
        return "Night Person";
    }

    @Override
    public void addTrait(FalloutCharacter fChar) {
        // do zorbienia i przemyślenia czy warto

    }

    @Override
    public void remTrait(FalloutCharacter fChar) {
        // do zorbienia i przemyślenia czy warto
    }

    @Override
    public String printTraitInfo() {
        return "<html>As a night-time person, you are more awake when the sun goes down. Your Intelligence and Perception " +
                "are improved at night but are dulled during the day. You get a 1 point penalty to these Statistics " +
                "from 0601 to 1800, and a 1 point bonus to these Stats from 1801 to 0600. Robots cannot choose this " +
                "trait. Note that the bonus cannot take IN and PE above the character’s racial maximum or below the " +
                "character’s racial minimum.</html>";
    }
}
