package com.atolsoft.rpgtool.util.traits.traits_specific;


import com.atolsoft.rpgtool.data.FalloutCharacter;

public class HamFisted extends Trait {

    @Override
    public String getName() {
        return "Ham fisted";
    }

    @Override
    public void addTrait(FalloutCharacter fChar) {

        fChar.getSkills().get(0).setSkillValue(fChar.getSkills().get(0).getSkillValue() -10);
        fChar.getSkills().get(3).setTagSkill(true);
    }

    @Override
    public void remTrait(FalloutCharacter fChar) {
        fChar.getSkills().get(0).setSkillValue(fChar.getSkills().get(0).getSkillValue() +10);
        fChar.getSkills().get(3).setTagSkill(false);
    }

    @Override
    public String printTraitInfo() {
        return "<html>Genetic engineering – or dumb luck – has endowed you with huge hands. You get a “free” tag skill " +
                "in Unarmed, but you suffer a -20% penalty to Small Guns, First Aid, Doctor, Repair, Science, and " +
                "Lockpick Skills (these numbers cannot go below 0%).</html>";
    }
}
