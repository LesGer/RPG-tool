package com.atolsoft.rpgtool.util.traits.traits_specific;


import com.atolsoft.rpgtool.data.FalloutCharacter;

public class Domesticated extends Trait {

    @Override
    public String getName() {
        return "Domesticated";
    }

    @Override
    public void addTrait(FalloutCharacter fChar) {
        fChar.getSpecial().get("IN").setSpecialValue(fChar.getSpecial().get("IN").getSpecialValue() + 1);
        fChar.getSecStats().get(4).setSecondarySkillValue(fChar.getSecStats().get(4).getSecondarySkillValue() - 2);
    }

    @Override
    public void remTrait(FalloutCharacter fChar) {
        fChar.getSpecial().get("IN").setSpecialValue(fChar.getSpecial().get("IN").getSpecialValue() - 1);
        fChar.getSecStats().get(4).setSecondarySkillValue(fChar.getSecStats().get(4).getSecondarySkillValue() + 2);
    }

    @Override
    public String printTraitInfo() {
        return "<html>You have undergone extensive house training and have developed an aboveaverage Intelligence. " +
                "Your IN is raised by 1, and can even go above your racial maximum, but you get a –2 penalty to " +
                "Melee Damage.</html>";
    }
}
