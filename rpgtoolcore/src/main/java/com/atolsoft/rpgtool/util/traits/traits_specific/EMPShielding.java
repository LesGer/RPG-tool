package com.atolsoft.rpgtool.util.traits.traits_specific;


import com.atolsoft.rpgtool.data.FalloutCharacter;

public class EMPShielding extends Trait {

    @Override
    public String getName() {
        return "EMP Shielding";
    }

    @Override
    public void addTrait(FalloutCharacter fChar) {
        fChar.getSecStats().get(8).setSecondarySkillValue(fChar.getSecStats().get(8).getSecondarySkillValue()+30);
        // modyfikator ruchu 1 hex  - 2ap
    }

    @Override
    public void remTrait(FalloutCharacter fChar) {
        fChar.getSecStats().get(8).setSecondarySkillValue(fChar.getSecStats().get(8).getSecondarySkillValue()+30);
        // usun modyfikator
    }

    @Override
    public String printTraitInfo() {
        return "<html>You have a dedicated EMP shielding system. It takes you 2 AP to move 1 hex because of your " +
                "heavy equipment, but you have a 30% Resistance to all forms of EMP attack.</html>";
    }
}
