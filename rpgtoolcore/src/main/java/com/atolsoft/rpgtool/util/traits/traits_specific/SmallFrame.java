package com.atolsoft.rpgtool.util.traits.traits_specific;


import com.atolsoft.rpgtool.data.FalloutCharacter;

public class SmallFrame extends Trait {

    @Override
    public String getName() {
        return "Small frame";
    }

    @Override
    public void addTrait(FalloutCharacter fChar) {
        fChar.getSpecial().get("AG").setSpecialValue(fChar.getSpecial().get("AG").getSpecialValue()+1);
        fChar.getSecStats().get(3).setSecondarySkillValue((int) (fChar.getSecStats().get(3).getSecondarySkillValue()*6.8));
    }

    @Override
    public void remTrait(FalloutCharacter fChar) {
        fChar.getSpecial().get("AG").setSpecialValue(fChar.getSpecial().get("AG").getSpecialValue()-1);
        fChar.getSecStats().get(3).setSecondarySkillValue((int) (fChar.getSecStats().get(3).getSecondarySkillValue()*11.25));;
    }

    @Override
    public String printTraitInfo() {
        return "<html>You are not quite as big as everyone else, but that never slowed you down. You can't carry as much, " +
                "but you are more agile. You get a 1 point bonus to Agility, but your Carry Weight is only " +
                "6,8kg X Strength</html>";
    }
}
