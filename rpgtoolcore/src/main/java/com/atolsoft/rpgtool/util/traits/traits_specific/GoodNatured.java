package com.atolsoft.rpgtool.util.traits.traits_specific;


import com.atolsoft.rpgtool.data.FalloutCharacter;

public class GoodNatured extends Trait {

    @Override
    public String getName() {
        return "Good Natured";
    }

    @Override
    public void addTrait(FalloutCharacter fChar) {
        fChar.getSkills().get("FA").setSkillValue(fChar.getSkills().get("FA").getSkillValue() + 20);
        fChar.getSkills().get("DC").setSkillValue(fChar.getSkills().get("DC").getSkillValue() + 20);
        fChar.getSkills().get(15).setSkillValue(fChar.getSkills().get(15).getSkillValue() + 20);
        fChar.getSkills().get(16).setSkillValue(fChar.getSkills().get(16).getSkillValue() + 20);

        fChar.getSkills().get("SG").setSkillValue(fChar.getSkills().get("SG").getSkillValue() - 10);
        fChar.getSkills().get("BG").setSkillValue(fChar.getSkills().get("BG").getSkillValue() - 10);
        fChar.getSkills().get(2).setSkillValue(fChar.getSkills().get(2).getSkillValue() - 10);
        fChar.getSkills().get(3).setSkillValue(fChar.getSkills().get(3).getSkillValue() - 10);
        fChar.getSkills().get(4).setSkillValue(fChar.getSkills().get(4).getSkillValue() - 10);

    }

    @Override
    public void remTrait(FalloutCharacter fChar) {
        fChar.getSkills().get(6).setSkillValue(fChar.getSkills().get(6).getSkillValue() - 20);
        fChar.getSkills().get(7).setSkillValue(fChar.getSkills().get(7).getSkillValue() - 20);
        fChar.getSkills().get(15).setSkillValue(fChar.getSkills().get(15).getSkillValue() - 20);
        fChar.getSkills().get(16).setSkillValue(fChar.getSkills().get(16).getSkillValue() - 20);

        fChar.getSkills().get(0).setSkillValue(fChar.getSkills().get(0).getSkillValue() + 10);
        fChar.getSkills().get(1).setSkillValue(fChar.getSkills().get(1).getSkillValue() + 10);
        fChar.getSkills().get(2).setSkillValue(fChar.getSkills().get(2).getSkillValue() + 10);
        fChar.getSkills().get(3).setSkillValue(fChar.getSkills().get(3).getSkillValue() + 10);
        fChar.getSkills().get(4).setSkillValue(fChar.getSkills().get(4).getSkillValue() + 10);
    }

    @Override
    public String printTraitInfo() {
        return "<html>You studied less-combative skills as you were growing up. Your combat skills start at a " +
                "lower level, but First Aid, Doctor, Speech, and Barter are substantially improved. Those skills " +
                "get a 20% bonus. You get a 10% penalty to starting combat skills (Small Guns, Big Guns, " +
                "Energy Weapons, Unarmed, and Melee Weapons). This is a one-time bonus.</html>";
    }
}
