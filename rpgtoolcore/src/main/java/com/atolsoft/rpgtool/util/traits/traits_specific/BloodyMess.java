package com.atolsoft.rpgtool.util.traits.traits_specific;


import com.atolsoft.rpgtool.data.FalloutCharacter;

public class BloodyMess extends Trait {

    @Override
    public String getName() {
        return "Bloody Mess";
    }

    @Override
    public void addTrait(FalloutCharacter fChar) {
        System.out.println("Bloody Mess");
    }

    @Override
    public void remTrait(FalloutCharacter fChar) {
        System.out.println("Bloody Mess");
    }

    @Override
    public String printTraitInfo() {
        return "<html>By some strange twist of fate, people around you die violently. You always " +
                "see the worst way a person can die. This does not mean you kill them any  " +
                "faster or slower, but when they do die, it will be dramatic.</html>";
    }
}
