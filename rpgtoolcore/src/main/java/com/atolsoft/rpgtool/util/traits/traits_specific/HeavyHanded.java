package com.atolsoft.rpgtool.util.traits.traits_specific;


import com.atolsoft.rpgtool.data.FalloutCharacter;

public class HeavyHanded extends Trait {

    @Override
    public String getName() {
        return "Heavy Handed";
    }

    @Override
    public void addTrait(FalloutCharacter fChar) {
        fChar.getSecStats().get(4).setSecondarySkillValue(fChar.getSecStats().get(4).getSecondarySkillValue() + 4);
        // crit dmg - 30%
        // crippling blow -30%
    }

    @Override
    public void remTrait(FalloutCharacter fChar) {
        fChar.getSecStats().get(4).setSecondarySkillValue(fChar.getSecStats().get(4).getSecondarySkillValue() - 4);
        // crit dmg - 30%
        // crippling blow -30%
    }

    @Override
    public String printTraitInfo() {
        return "<html>You swing harder, not better. Your attacks are very brutal, but lack finesse. You rarely cause a " +
                "good critical hit, but you always do more melee damage. You get a 4 point bonus to Melee Damage," +
                " but your critical hits do 30% less damage, and are 30% less likely to cripple a limb or cause " +
                "unconsciousness.</html>";
    }
}
