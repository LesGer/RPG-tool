package com.atolsoft.rpgtool.util.traits.traits_specific;


import com.atolsoft.rpgtool.data.FalloutCharacter;

public class Bruisher extends Trait {


    @Override
    public String getName() {
        return "Bruisher";
    }

    @Override
    public void addTrait(FalloutCharacter fChar) {
        fChar.getSpecial().get("ST").setSpecialValue(fChar.getSpecial().get("ST").getSpecialValue()+2);
        fChar.getSecStats().get(2).setSecondarySkillValue(fChar.getSecStats().get(2).getSecondarySkillValue()-2);
    }

    @Override
    public void remTrait(FalloutCharacter fChar) {
        fChar.getSpecial().get("ST").setSpecialValue(fChar.getSpecial().get("ST").getSpecialValue()-2);
        fChar.getSecStats().get(2).setSecondarySkillValue(fChar.getSecStats().get(2).getSecondarySkillValue()+2);
    }

    @Override
    public String printTraitInfo() {
        return "<html>A little slower, but a little bigger. You may not hit as often, but they will " +
                "feel it when you do! Your total action points are lowered, but your Strength is " +
                "increased. You get a 2 point bonus to Strength, but loose 2 Action Points.</html>";
    }
}
