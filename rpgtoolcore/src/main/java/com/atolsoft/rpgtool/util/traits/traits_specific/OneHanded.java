package com.atolsoft.rpgtool.util.traits.traits_specific;


import com.atolsoft.rpgtool.data.FalloutCharacter;

public class OneHanded extends Trait {

    @Override
    public String getName() {
        return "One Handed";
    }

    @Override
    public void addTrait(FalloutCharacter fChar) {
            System.out.println("Nie można wybrać tej biegłości dla wybranej rasy");
        // bonus 40% do broni jednoręcznej
        // nodyfikator -20 dla broni 2-ręcznych
    }

    @Override
    public void remTrait(FalloutCharacter fChar) {
            System.out.println("Nie można wybrać tej biegłości dla wybranej rasy");

        // bonus 40% do broni jednoręcznej
        // nodyfikator -20 dla broni 2-ręcznych
    }

    @Override
    public String printTraitInfo() {
        return  "<html>One of your hands is very dominant. You excel with single-handed weapons, but two-handed weapons " +
                "cause a problem. You have a 40% penalty to hit with twohanded weapons, but get a 20% bonus to hit" +
                " with weapons that only require one hand.</html>";
    }
}
