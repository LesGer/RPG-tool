package com.atolsoft.rpgtool.util.traits.traits_specific;


import com.atolsoft.rpgtool.data.FalloutCharacter;

public class ChemResistant extends Trait {

    @Override
    public String getName() {
        return "Chem Resistant";
    }

    @Override
    public void addTrait(FalloutCharacter fChar) {
        System.out.println("Chem resistant");

    }

    @Override
    public void remTrait(FalloutCharacter fChar) {

        System.out.println("Chem resistant");
    }

    @Override
    public String printTraitInfo() {
        return "<html>Chems only effect you half as long as normal, but your chance to be addicted is only " +
                "50% the normal amount.</html>";
    }
}
