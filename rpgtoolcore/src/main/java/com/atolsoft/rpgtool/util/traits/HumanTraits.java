package com.atolsoft.rpgtool.util.traits;


import com.atolsoft.rpgtool.util.traits.traits_specific.*;

import java.util.ArrayList;

public class HumanTraits extends TraitsManager {

    public HumanTraits() {
        setTraits(new ArrayList<>());
        getTraits().add(new FastMetabolism());
        getTraits().add(new Bruisher());
        getTraits().add(new SmallFrame());
        getTraits().add(new Finesse());
        getTraits().add(new Kamikaze());
        getTraits().add(new HeavyHanded());
        getTraits().add(new BloodyMess());
        getTraits().add(new Jinxed());
        getTraits().add(new GoodNatured());
        getTraits().add(new ChemReliant());
        getTraits().add(new ChemResistant());
        getTraits().add(new NightPerson());
        getTraits().add(new Skilled());
        getTraits().add(new SexAppeal());
        getTraits().add(new TechWizzard());
    }

}
