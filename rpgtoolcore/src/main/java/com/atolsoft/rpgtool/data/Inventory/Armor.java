package com.atolsoft.rpgtool.data.Inventory;

public class Armor extends Item{

    private int acMod;
    private int normalDamageT, normalDamageMod;
    private int laserDamageT, laserDamageMod;
    private int fireDamageT, fireDamageMod;
    private int plasmaDamageT, plasmaDamageMod;
    private int explosionDamageT, explosionDamageMod;


    public Armor(String name, double weight, int value, int acMod, int normalDT, int normalDM, int lasetDT, int laserDM,
                 int fireDT, int fireDM, int plasmaDT, int plasmaDM, int explosionDT, int explosionDM){
        super(name,weight,value);
        this.acMod = acMod;
        this.normalDamageT = normalDT;
        this.normalDamageMod = normalDM;
        this.laserDamageT = lasetDT;
        this.laserDamageMod = laserDM;
        this.fireDamageT = fireDT;
        this.fireDamageMod = fireDM;
        this.plasmaDamageT = plasmaDT;
        this.plasmaDamageMod = plasmaDM;
        this.explosionDamageT = explosionDT;
        this.explosionDamageMod = explosionDM;

    }

    public int getAcMod() {
        return acMod;
    }

    public void setAcMod(int acMod) {
        this.acMod = acMod;
    }

    public int getNormalDamageT() {
        return normalDamageT;
    }

    public void setNormalDamageT(int normalDamageT) {
        this.normalDamageT = normalDamageT;
    }

    public int getNormalDamageMod() {
        return normalDamageMod;
    }

    public void setNormalDamageMod(int normalDamageMod) {
        this.normalDamageMod = normalDamageMod;
    }

    public int getLaserDamageT() {
        return laserDamageT;
    }

    public void setLaserDamageT(int laserDamageT) {
        this.laserDamageT = laserDamageT;
    }

    public int getLaserDamageMod() {
        return laserDamageMod;
    }

    public void setLaserDamageMod(int laserDamageMod) {
        this.laserDamageMod = laserDamageMod;
    }

    public int getFireDamageT() {
        return fireDamageT;
    }

    public void setFireDamageT(int fireDamageT) {
        this.fireDamageT = fireDamageT;
    }

    public int getFireDamageMod() {
        return fireDamageMod;
    }

    public void setFireDamageMod(int fireDamageMod) {
        this.fireDamageMod = fireDamageMod;
    }

    public int getPlasmaDamageT() {
        return plasmaDamageT;
    }

    public void setPlasmaDamageT(int plasmaDamageT) {
        this.plasmaDamageT = plasmaDamageT;
    }

    public int getPlasmaDamageMod() {
        return plasmaDamageMod;
    }

    public void setPlasmaDamageMod(int plasmaDamageMod) {
        this.plasmaDamageMod = plasmaDamageMod;
    }

    public int getExplosionDamageT() {
        return explosionDamageT;
    }

    public void setExplosionDamageT(int explosionDamageT) {
        this.explosionDamageT = explosionDamageT;
    }

    public int getExplosionDamageMod() {
        return explosionDamageMod;
    }

    public void setExplosionDamageMod(int explosionDamageMod) {
        this.explosionDamageMod = explosionDamageMod;
    }

    @Override
    public String toString() {
        return "Armor{" + getItemName() + " " + getItemWeigh() + " " + getItemValue() +
                " acMod=" + acMod +
                ", normalDamageT=" + normalDamageT +
                ", normalDamageMod=" + normalDamageMod +
                ", laserDamageT=" + laserDamageT +
                ", laserDamageMod=" + laserDamageMod +
                ", fireDamageT=" + fireDamageT +
                ", fireDamageMod=" + fireDamageMod +
                ", plasmaDamageT=" + plasmaDamageT +
                ", plasmaDamageMod=" + plasmaDamageMod +
                ", explosionDamageT=" + explosionDamageT +
                ", explosionDamageMod=" + explosionDamageMod +
                '}';
    }

    @Override
    public int damage(int weaponDamage) {
        return 0;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Armor armor = (Armor) o;

        if (acMod != armor.acMod) return false;
        if (normalDamageT != armor.normalDamageT) return false;
        if (normalDamageMod != armor.normalDamageMod) return false;
        if (laserDamageT != armor.laserDamageT) return false;
        if (laserDamageMod != armor.laserDamageMod) return false;
        if (fireDamageT != armor.fireDamageT) return false;
        if (fireDamageMod != armor.fireDamageMod) return false;
        if (plasmaDamageT != armor.plasmaDamageT) return false;
        if (plasmaDamageMod != armor.plasmaDamageMod) return false;
        if (explosionDamageT != armor.explosionDamageT) return false;
        return explosionDamageMod == armor.explosionDamageMod;
    }

    @Override
    public int hashCode() {
        int result = acMod;
        result = 31 * result + normalDamageT;
        result = 31 * result + normalDamageMod;
        result = 31 * result + laserDamageT;
        result = 31 * result + laserDamageMod;
        result = 31 * result + fireDamageT;
        result = 31 * result + fireDamageMod;
        result = 31 * result + plasmaDamageT;
        result = 31 * result + plasmaDamageMod;
        result = 31 * result + explosionDamageT;
        result = 31 * result + explosionDamageMod;
        return result;
    }
}
