package com.atolsoft.rpgtool.data.skills;

import com.atolsoft.rpgtool.data.FalloutCharacter;
import com.atolsoft.rpgtool.data.names.SkillsName;

public class Pilot extends Skills {

    public Pilot() {
        super();
        setSkillName(SkillsName.PILOT.getSkillName());
        setSkillKey(SkillsName.PILOT.getSkillkey());
    }

    @Override
    public int countSkill(FalloutCharacter fChar) {
        return (2 * getCurrentSpecial(fChar, "PE") + getCurrentSpecial(fChar, "AG"));
    }
}
