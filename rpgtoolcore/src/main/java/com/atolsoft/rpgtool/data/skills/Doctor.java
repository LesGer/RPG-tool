package com.atolsoft.rpgtool.data.skills;

import com.atolsoft.rpgtool.data.FalloutCharacter;
import com.atolsoft.rpgtool.data.names.SkillsName;

public class Doctor extends Skills {

    public Doctor() {
        super();
        setSkillName(SkillsName.DOCTOR.getSkillName());
        setSkillKey(SkillsName.DOCTOR.getSkillkey());
    }

    @Override
    public int countSkill(FalloutCharacter fChar) {
        return getCurrentSpecial(fChar, "PE") + getCurrentSpecial(fChar, "IN");
    }
}
