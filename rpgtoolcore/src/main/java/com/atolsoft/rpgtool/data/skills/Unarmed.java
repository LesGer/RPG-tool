package com.atolsoft.rpgtool.data.skills;

import com.atolsoft.rpgtool.data.FalloutCharacter;
import com.atolsoft.rpgtool.data.names.SkillsName;

public class Unarmed extends Skills {

    public Unarmed(){
        super();
        setSkillName(SkillsName.UNARMED.getSkillName());
        setSkillKey(SkillsName.UNARMED.getSkillkey());
    }

    @Override
    public int countSkill(FalloutCharacter fChar) {
        return (2*(getCurrentSpecial(fChar,"AG") + (getCurrentSpecial(fChar,"ST"))));
    }
}
