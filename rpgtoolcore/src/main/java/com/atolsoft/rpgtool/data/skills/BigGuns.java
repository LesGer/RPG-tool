package com.atolsoft.rpgtool.data.skills;

import com.atolsoft.rpgtool.data.FalloutCharacter;
import com.atolsoft.rpgtool.data.names.SkillsName;

public class BigGuns extends Skills {

    public BigGuns(){
        super();
        setSkillName(SkillsName.BIG_GUNS.getSkillName());
        setSkillKey(SkillsName.BIG_GUNS.getSkillkey());
    }

    @Override
    public int countSkill(FalloutCharacter fChar) {
            return (getCurrentSpecial(fChar,"AG")*2);
    }
}
