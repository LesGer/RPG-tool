package com.atolsoft.rpgtool.data.names;

public enum SecondaryStatsName {
        HIT_POINTS("HP","Hit Points"),
        ARMOR_CLASS("AC","Armor Class"),
        ACTION_POINTS("AP","Action Points"),
        CARRY_WEIGHT("CW","Carry Weight"),
        MELEE_DAMAGE("MD","Melee Damage"),
        POISON_RES("PR","Poison Resistance"),
        RADIATION_RES("RR","Radiation Resistance"),
        GAS_RES("GS","Gas Resistance"),
        ELECTRICAL_RES("ER","Electrical Resistance"),
        DAMAGE_RES("DR","Damage Resistance"),
        SEQUENCE("Seq","Sequence"),
        HEALING_RATE("HR","Healing Rate"),
        CRITICAL_CHANCE("Crit","Critical Chance");

        String shortName;
        String secondaryStatName;

    public String getShortName() {
        return shortName;
    }

    public String getSecondaryStatName() {
        return secondaryStatName;
    }

    SecondaryStatsName(String shortName, String name){
        this.shortName = shortName;
        this.secondaryStatName = name;
    }
}
