package com.atolsoft.rpgtool.data.Inventory;

import java.util.ArrayList;
import java.util.List;

public class InventoryList {

    private List<Item> inventory;

    public InventoryList(){
        this.inventory = new ArrayList<>();
    }

    public List<Item> getInventory() {
        return inventory;
    }

    public void setInventory(List<Item> inventory) {
        this.inventory = inventory;
    }
}
