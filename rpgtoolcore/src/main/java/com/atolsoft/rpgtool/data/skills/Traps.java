package com.atolsoft.rpgtool.data.skills;

import com.atolsoft.rpgtool.data.FalloutCharacter;
import com.atolsoft.rpgtool.data.names.SkillsName;

public class Traps extends Skills {

    public Traps() {
        super();
        setSkillName(SkillsName.TRAPS.getSkillName());
        setSkillKey(SkillsName.TRAPS.getSkillkey());
    }

    @Override
    public int countSkill(FalloutCharacter fChar) {
        return 10 + getCurrentSpecial(fChar, "PE");
    }
}
