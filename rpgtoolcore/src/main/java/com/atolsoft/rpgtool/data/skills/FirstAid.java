package com.atolsoft.rpgtool.data.skills;

import com.atolsoft.rpgtool.data.FalloutCharacter;
import com.atolsoft.rpgtool.data.names.SkillsName;

public class FirstAid extends Skills {

    public FirstAid() {
        super();
        setSkillName(SkillsName.FIRST_AID.getSkillName());
        setSkillKey(SkillsName.FIRST_AID.getSkillkey());
    }

    @Override
    public int countSkill(FalloutCharacter fChar) {
        return (2 * (getCurrentSpecial(fChar, "PE") + getCurrentSpecial(fChar, "IN")));
    }
}
