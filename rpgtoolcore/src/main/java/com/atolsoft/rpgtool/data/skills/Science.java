package com.atolsoft.rpgtool.data.skills;

import com.atolsoft.rpgtool.data.FalloutCharacter;
import com.atolsoft.rpgtool.data.names.SkillsName;

public class Science extends Skills {

    public Science() {
        super();
        setSkillName(SkillsName.SCIENCE.getSkillName());
        setSkillKey(SkillsName.SCIENCE.getSkillkey());
    }

    @Override
    public int countSkill(FalloutCharacter fChar) {
        return 4 * getCurrentSpecial(fChar, "IN");
    }
}
