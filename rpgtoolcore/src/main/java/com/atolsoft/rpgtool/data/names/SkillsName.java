package com.atolsoft.rpgtool.data.names;

public enum SkillsName {

    SMALL_GUNS("SG","Small guns"),
    BIG_GUNS("BG","Big guns"),
    ENERGY_WEAPONS("EW","Energy weapons"),
    UNARMED("UN","Unarmed"),
    MELEE_WEAPONS("MW","Melee Weapons"),
    THROWING("TH","Throwing"),
    FIRST_AID("FA","First Aid"),
    DOCTOR("DC","Doctor"),
    SNEAK("SN","Sneak"),
    LOCKPICK("LC","Lockpick"),
    STEAL("ST","Steal"),
    TRAPS("TR","Traps"),
    SCIENCE("SC","Science"),
    REPAIR("RE","Repair"),
    PILOT("PI","Pilot"),
    SPEECH("SP","Speech"),
    BARTER("BA","Barter"),
    GAMBLING("GA","Gambling"),
    OUTDORSMAN("OU","Outdorsman");


    private String skillName;
    private String skillkey;


    public String getSkillkey() {
        return skillkey;
    }

    public String getSkillName() {
        return skillName;
    }

    SkillsName(String key, String desc) {
        this.skillkey = key;
        this.skillName = desc;
    }

    @Override
    public String toString() {
        return getSkillName();
    }
}
