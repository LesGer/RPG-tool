package com.atolsoft.rpgtool.data.skills;

import com.atolsoft.rpgtool.data.FalloutCharacter;
import com.atolsoft.rpgtool.data.names.SkillsName;

public class Repair extends Skills {

    public Repair() {
        super();
        setSkillName(SkillsName.REPAIR.getSkillName());
        setSkillKey(SkillsName.REPAIR.getSkillkey());
    }

    @Override
    public int countSkill(FalloutCharacter fChar) {
        return 3 * getCurrentSpecial(fChar, "IN");
    }
}
