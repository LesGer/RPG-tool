package com.atolsoft.rpgtool.data.names;

public enum XPLevels {
    LEVEL1(0),
    LEVEL2(1000),
    LEVEL3(3000),
    LEVEL4(6000),
    LEVEL5(10000),
    LEVEL6(15000),
    LEVEL7(21000),
    LEVEL8(28000),
    LEVEL9(36000),
    LEVEL10(45000),
    LEVEL11(55000),
    LEVEL12(66000),
    LEVEL13(78000),
    LEVEL14(91000),
    LEVEL15(105000),
    LEVEL16(120000),
    LEVEL17(135000),
    LEVEL18(153000),
    LEVEL19(171000),
    LEVEL20(190000),
    LEVEL21(210000),
    LEVEL22(250000),
    LEVEL23(290000),
    LEVEL24(330000),
    LEVEL25(370000)
    ;


    private int xpLevel;

    public int getXpLevel() {
        return xpLevel;
    }

    XPLevels(int xp) {
        this.xpLevel = xp;
    }
}
