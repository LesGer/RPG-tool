package com.atolsoft.rpgtool.data;

import java.util.Map;

public class Ghul extends FalloutCharacter {

    private static final long serialVersionUID = -2081993906213980762L;

    public Ghul(String name, int age, int level, int xp) {
        super(name, age, level, xp);
        setRace("Ghul");
        setFreeSpecialPoints(5);
        putSpecialInArray();
        specialValuesSetter();
        putSkillsInArray();
        putSecondaryStatsInArray();
    }

    @Override
    public void specialValuesSetter(){
        for(Map.Entry<String,Special> entry : getSpecial().entrySet()){
            entry.getValue().setSpecialValue(5);
        }
        getSpecial().get("ST").setSpecialMinValue(1);
        getSpecial().get("ST").setSpecialMaxValue(8);
        getSpecial().get("PE").setSpecialMinValue(4);
        getSpecial().get("PE").setSpecialMaxValue(14);
        getSpecial().get("EN").setSpecialMinValue(1);
        getSpecial().get("EN").setSpecialMaxValue(10);
        getSpecial().get("CH").setSpecialMinValue(1);
        getSpecial().get("CH").setSpecialMaxValue(10);
        getSpecial().get("IN").setSpecialMinValue(2);
        getSpecial().get("IN").setSpecialMaxValue(10);
        getSpecial().get("AG").setSpecialMinValue(1);
        getSpecial().get("AG").setSpecialMaxValue(6);
        getSpecial().get("LK").setSpecialMinValue(5);
        getSpecial().get("LK").setSpecialMaxValue(12);
    }

    @Override
    public void addRaceMods(){
        getSecStats().get(5).setSecondarySkillValue(getSecStats().get(5).getSecondarySkillValue() + 30);
        getSecStats().get(6).setSecondarySkillValue(getSecStats().get(6).getSecondarySkillValue() + 80);
    }
}
