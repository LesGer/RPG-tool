package com.atolsoft.rpgtool.data.skills;

import com.atolsoft.rpgtool.data.FalloutCharacter;
import com.atolsoft.rpgtool.data.names.SkillsName;

public class Outdorsman extends Skills {

    public Outdorsman() {
        super();
        setSkillName(SkillsName.OUTDORSMAN.getSkillName());
        setSkillKey(SkillsName.OUTDORSMAN.getSkillkey());
    }

    @Override
    public int countSkill(FalloutCharacter fChar) {
        return (2 * (getCurrentSpecial(fChar, "EN") + getCurrentSpecial(fChar, "IN")));
    }
}
