package com.atolsoft.rpgtool.data.Inventory;

public abstract class Item {

    private String itemName;
    private double itemWeigh;
    private int itemValue;

    public Item(String itemName, double itemWeigh, int value) {
        this.itemName = itemName;
        this.itemWeigh = itemWeigh;
        this.itemValue = value;
    }

    public int valueMod(int shopBarterValue, int characterBarterValue) {
        if(shopBarterValue < 0) {
            throw new IllegalArgumentException("Złe wartości");
        }
        if(characterBarterValue <= 0) {
            throw new IllegalArgumentException("Coś poszło nie tak przy inicjalizacji postaci");
        }
        double value = this.itemValue;

        if(shopBarterValue == 0) {
            value =  value * (1- (double) characterBarterValue/100);
            return (int) value;
        }

        value += value * (shopBarterValue - characterBarterValue)/100;


        return (int) value;
    }

    public abstract int damage(int weaponDamage);

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public double getItemWeigh() {
        return itemWeigh;
    }

    public void setItemWeigh(int itemWeigh) {
        this.itemWeigh = itemWeigh;
    }

    public int getItemValue() {
        return itemValue;
    }

    public void setItemValue(int itemValue) {
        this.itemValue = itemValue;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Item item = (Item) o;

        if (Double.compare(item.itemWeigh, itemWeigh) != 0) return false;
        if (itemValue != item.itemValue) return false;
        return itemName != null ? itemName.equals(item.itemName) : item.itemName == null;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = itemName != null ? itemName.hashCode() : 0;
        temp = Double.doubleToLongBits(itemWeigh);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + itemValue;
        return result;
    }
}
