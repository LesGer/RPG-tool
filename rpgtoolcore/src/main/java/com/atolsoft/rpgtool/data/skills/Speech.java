package com.atolsoft.rpgtool.data.skills;

import com.atolsoft.rpgtool.data.FalloutCharacter;
import com.atolsoft.rpgtool.data.names.SkillsName;

public class Speech extends Skills {

    public Speech() {
        super();
        setSkillName(SkillsName.SPEECH.getSkillName());
        setSkillKey(SkillsName.SPEECH.getSkillkey());
    }

    @Override
    public int countSkill(FalloutCharacter fChar) {
        return (5 + (5 * getCurrentSpecial(fChar, "CH")));
    }
}
