package com.atolsoft.rpgtool.data.skills;

import com.atolsoft.rpgtool.data.FalloutCharacter;
import com.atolsoft.rpgtool.data.names.SkillsName;

public class Lockpick extends Skills {

    public Lockpick() {
        super();
        setSkillName(SkillsName.LOCKPICK.getSkillName());
        setSkillKey(SkillsName.LOCKPICK.getSkillkey());
    }

    @Override
    public int countSkill(FalloutCharacter fChar) {
        return 10 + getCurrentSpecial(fChar, "AG") + getCurrentSpecial(fChar, "PE");
    }
}
