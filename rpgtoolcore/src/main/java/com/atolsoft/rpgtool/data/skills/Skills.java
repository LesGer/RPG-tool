package com.atolsoft.rpgtool.data.skills;

import com.atolsoft.rpgtool.data.FalloutCharacter;

public abstract class Skills {


    private String skillName;
    private String skillKey;
    private int skillValue;
    private boolean tagSkill;
    private String skillInfo;

    public String getSkillName() {
        return skillName;
    }

    public void setSkillName(String skillName) {
        this.skillName = skillName;
    }

    public int getSkillValue() {
        return skillValue;
    }

    public void setSkillValue(int skillValue) {
        this.skillValue = skillValue;
    }

    public String getSkillKey() {
        return skillKey;
    }

    public void setSkillKey(String skillKey) {
        this.skillKey = skillKey;
    }

    public boolean isTagSkill() {
        return tagSkill;
    }

    public void setTagSkill(boolean tagSkill) {
        this.tagSkill = tagSkill;
    }

    public Skills() {
        this.tagSkill = false;
    }

    public String getSkillInfo() {
        return skillInfo;
    }

    public void setSkillInfo(String skillInfo) {
        this.skillInfo = skillInfo;
    }

    public abstract int countSkill(FalloutCharacter fChar);

    protected int getCurrentSpecial(FalloutCharacter fChar, String key) {
        return fChar.getSpecial().get(key).getSpecialValue();
    }

    public void setCountedSkillValue(FalloutCharacter fChar) {
            setSkillValue(tagSkill(fChar) + skilled(fChar) + countSkill(fChar));
    }

    private int tagSkill(FalloutCharacter fChar) {
        if (this.tagSkill) {
            System.out.println("trait");
            return 20;
        }
        return 0;
    }

    private int skilled(FalloutCharacter fChar) {
        if (fChar.getTraitsManager().getTraits().get(12).isActiveTrait()) {
            System.out.println("skilled");
            return 10;

        }
        return 0;
    }

    @Override
    public String toString() {
        return getSkillName() + " " + getSkillValue();
    }
}
