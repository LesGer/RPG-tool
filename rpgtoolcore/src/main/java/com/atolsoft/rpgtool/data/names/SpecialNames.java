package com.atolsoft.rpgtool.data.names;

public enum SpecialNames {
    STRENGTH    ("ST","Strength"),
    PERCEPTION  ("PE","Perception"),
    ENDURANCE   ("EN","Endurance"),
    CHARISMA    ("CH","Charisma"),
    INTELLIGENCE("IN","Intelligence"),
    AGILITY     ("AG","Agility"),
    LUCK        ("LK","Luck");

    private String specialKey;

    private String specialName;

    public String getSpecialName() {
        return specialName;
    }

    public String getSpecialKey(){
        return specialKey;
    }

    SpecialNames(String key, String names) {
        this.specialKey = key;
        this.specialName = names;
    }
}
