package com.atolsoft.rpgtool.data;



import com.atolsoft.rpgtool.data.names.SecondaryStatsName;
import com.atolsoft.rpgtool.data.names.SkillsName;
import com.atolsoft.rpgtool.data.names.SpecialNames;
import com.atolsoft.rpgtool.data.skills.*;
import com.atolsoft.rpgtool.util.traits.HumanTraits;
import com.atolsoft.rpgtool.util.traits.TraitsManager;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

public class FalloutCharacter implements Serializable {
    private static final long serialVersionUID = -525542791075714404L;
    private String name;                                                // imie postaci
    private int age;                                                    // wiek postaci
    private int level;                                                  // poziom postaci
    private int xp;                                                     // punkty xp postaci
    private Map<String, Special> special = new LinkedHashMap<>();            // zmienne atrybutów;
    private Map<String, Skills> skills = new LinkedHashMap<>();         // skille
    private int freeSpecialPoints;                                      // wolne punkty special
    private ArrayList<SecondaryStats> secStats = new ArrayList<>();     // PŻ, sekwencja, odporności itp.
    private TraitsManager traitsManager;
    private String race;
    private int tagSkillCounter;

    public Map<String, Special> getSpecial() {
        return special;
    }

    public ArrayList<SecondaryStats> getSecStats() {
        return secStats;
    }

    public Map<String, Skills> getSkills() {
        return skills;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getXp() {
        return xp;
    }

    public void setXp(int xp) {
        this.xp = xp;
    }

    public String getRace() {
        return race;
    }

    public void setRace(String race) {
        this.race = race;
    }

    public int getFreeSpecialPoints() {
        return freeSpecialPoints;
    }

    public void setFreeSpecialPoints(int freeSpecialPoints) {
        this.freeSpecialPoints = freeSpecialPoints;
    }

    public int getTagSkillCounter() {
        return tagSkillCounter;
    }

    public FalloutCharacter() {
        setFreeSpecialPoints(5);
        putSpecialInArray();
        putSkillsInArray();
        putSecondaryStatsInArray();
        specialValuesSetter();
        tagSkillCounter = 3;
        traitsManager = new HumanTraits();
    }

    public FalloutCharacter(String name, int age, int level, int xp) {
        setName(name);
        setAge(age);
        setLevel(level);
        setXp(xp);
        setFreeSpecialPoints(5);
        putSpecialInArray();
        putSkillsInArray();
        putSecondaryStatsInArray();
        tagSkillCounter = 3;
    }

    protected void putSpecialInArray() {
        for (int i = 0; i < SpecialNames.values().length; i++) {
            special.put(SpecialNames.values()[i].getSpecialKey(), new Special(SpecialNames.values()[i].getSpecialName()));
        }
    }

    protected void putSecondaryStatsInArray() {
        for (int i = 0; i < SecondaryStatsName.values().length; i++) {
            getSecStats().add(new SecondaryStats(SecondaryStatsName.values()[i].getSecondaryStatName()));
        }
    }

    protected void putSkillsInArray() {
        skills.put(SkillsName.SMALL_GUNS.getSkillkey(),new SmallGuns());
        skills.put(SkillsName.BIG_GUNS.getSkillkey(),new BigGuns());
        skills.put(SkillsName.ENERGY_WEAPONS.getSkillkey(),new EnergyWeapons());
        skills.put(SkillsName.UNARMED.getSkillkey(),new Unarmed());
        skills.put(SkillsName.MELEE_WEAPONS.getSkillkey(),new MeleeWeapons());
        skills.put(SkillsName.THROWING.getSkillkey(),new Throwing());
        skills.put(SkillsName.FIRST_AID.getSkillkey(),new FirstAid());
        skills.put(SkillsName.DOCTOR.getSkillkey(),new Doctor());
        skills.put(SkillsName.SNEAK.getSkillkey(),new Sneak());
        skills.put(SkillsName.LOCKPICK.getSkillkey(),new Lockpick());
        skills.put(SkillsName.STEAL.getSkillkey(),new Steal());
        skills.put(SkillsName.TRAPS.getSkillkey(),new Traps());
        skills.put(SkillsName.SCIENCE.getSkillkey(),new Science());
        skills.put(SkillsName.REPAIR.getSkillkey(),new Repair());
        skills.put(SkillsName.PILOT.getSkillkey(),new Pilot());
        skills.put(SkillsName.SPEECH.getSkillkey(),new Speech());
        skills.put(SkillsName.BARTER.getSkillkey(),new Barter());
        skills.put(SkillsName.GAMBLING.getSkillkey(),new Gambling());
        skills.put(SkillsName.OUTDORSMAN.getSkillkey(),new Outdorsman());
    }

    public void specialValuesSetter() {
        for (Map.Entry<String, Special> entry : getSpecial().entrySet()) {
            entry.getValue().setSpecialValue(5);
            entry.getValue().setSpecialMaxValue(10);
            entry.getValue().setSpecialMinValue(1);
        }

    }

    public void addRaceMods() {

    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();

        builder.append(String.format("Imię postaci: %-15s | Rasa: %10s | Wiek: %4d | Poziom: %4d | XP: %10d |",
                getName(), getRace(), getAge(), getLevel(), getXp()));

        return builder.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FalloutCharacter that = (FalloutCharacter) o;

        if (age != that.age) return false;
        if (level != that.level) return false;
        if (xp != that.xp) return false;
        if (freeSpecialPoints != that.freeSpecialPoints) return false;
        if (!name.equals(that.name)) return false;
        if (!special.equals(that.special)) return false;
        if (!skills.equals(that.skills)) return false;
        if (!secStats.equals(that.secStats)) return false;
        return race.equals(that.race);
    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + age;
        result = 31 * result + level;
        result = 31 * result + xp;
        result = 31 * result + special.hashCode();
        result = 31 * result + skills.hashCode();
        result = 31 * result + freeSpecialPoints;
        result = 31 * result + secStats.hashCode();
        result = 31 * result + race.hashCode();
        return result;
    }

    public TraitsManager getTraitsManager() {
        return traitsManager;
    }

    public void setTraitsManager(TraitsManager traitsManager) {
        this.traitsManager = traitsManager;
    }
}

