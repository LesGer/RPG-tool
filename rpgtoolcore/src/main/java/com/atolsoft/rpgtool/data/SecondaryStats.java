package com.atolsoft.rpgtool.data;

public class SecondaryStats {
    private String secondarySkillName;
    private int secondarySkillValue;

    public String getSecondarySkillName() {
        return secondarySkillName;
    }

    public void setSecondarySkillName(String secondarySkillName) {
        this.secondarySkillName = secondarySkillName;
    }

    public int getSecondarySkillValue() {
        return secondarySkillValue;
    }

    public void setSecondarySkillValue(int secondarySkillValue) {
        this.secondarySkillValue = secondarySkillValue;
    }

    public SecondaryStats(String name) {
        setSecondarySkillName(name);
    }
}
