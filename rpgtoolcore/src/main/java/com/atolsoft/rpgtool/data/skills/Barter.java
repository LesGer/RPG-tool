package com.atolsoft.rpgtool.data.skills;

import com.atolsoft.rpgtool.data.FalloutCharacter;
import com.atolsoft.rpgtool.data.names.SkillsName;

public class Barter extends Skills {

    public Barter() {
        super();
        setSkillName(SkillsName.BARTER.getSkillName());
        setSkillKey(SkillsName.BARTER.getSkillkey());
    }

    @Override
    public int countSkill(FalloutCharacter fChar) {
        return (4 * getCurrentSpecial(fChar, "CH"));
    }
}
