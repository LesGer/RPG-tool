package com.atolsoft.rpgtool.data.skills;

import com.atolsoft.rpgtool.data.FalloutCharacter;
import com.atolsoft.rpgtool.data.names.SkillsName;

public class EnergyWeapons extends Skills {

    public EnergyWeapons(){
        super();
        setSkillName(SkillsName.ENERGY_WEAPONS.getSkillName());
        setSkillKey(SkillsName.ENERGY_WEAPONS.getSkillkey());
    }

    @Override
    public int countSkill(FalloutCharacter fChar) {
        return (getCurrentSpecial(fChar,"AG")*2);
    }
}
