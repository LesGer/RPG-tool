package com.atolsoft.rpgtool.data.skills;

import com.atolsoft.rpgtool.data.FalloutCharacter;
import com.atolsoft.rpgtool.data.names.SkillsName;

public class Throwing extends Skills {

    public Throwing() {
        super();
        setSkillName(SkillsName.THROWING.getSkillName());
        setSkillKey(SkillsName.THROWING.getSkillkey());
    }

    @Override
    public int countSkill(FalloutCharacter fChar) {
        return (getCurrentSpecial(fChar, "AG")*4);
    }
}
