package com.atolsoft.rpgtool.data.skills;

import com.atolsoft.rpgtool.data.FalloutCharacter;
import com.atolsoft.rpgtool.data.names.SkillsName;

public class MeleeWeapons extends Skills {

    public MeleeWeapons() {
        super();
        setSkillName(SkillsName.MELEE_WEAPONS.getSkillName());
        setSkillKey(SkillsName.MELEE_WEAPONS.getSkillkey());
    }

    @Override
    public int countSkill(FalloutCharacter fChar) {
        return (20 + (2 * getCurrentSpecial(fChar, "AG") + getCurrentSpecial(fChar, "ST")));
    }
}
