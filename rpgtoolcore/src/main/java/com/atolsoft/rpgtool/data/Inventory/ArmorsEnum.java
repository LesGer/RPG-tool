package com.atolsoft.rpgtool.data.Inventory;

public enum ArmorsEnum {

    NONE("--- NONE ---", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
    LEATHER_JACKET("Leather Jacket", 2.25, 250, 8, 0, 20, 0, 20, 0, 10, 0, 10, 0, 20),
    LEATHER_ARMOR("Leather Armor", 3.5, 700, 15, 2, 25, 0, 20, 0, 20, 0, 10, 0, 20),
    COMBAT_LEATHER_JACKET("Combat Leather Jacket", 3, 1000, 20, 2, 30, 0, 20, 2, 25, 0, 10, 0, 20),
    LEATHER_ARMOR_MKII("Leather Armor MK II", 3.6, 1000, 20, 3, 25, 1, 30, 1, 17, 1, 30, 1, 25),
    METAL_ARMOR("Metal Armor", 15.75, 1100, 10, 4, 30, 6, 75, 4, 10, 4, 20, 4, 25),
    METAL_ARMOR_MKII("Metal Armor MK II", 15.75, 1900, 15, 4, 35, 7, 80, 4, 15, 4, 25, 4, 30),
    TESLA_ARMOR("Tesla Armor", 15.75, 4500, 15, 4, 20, 19, 90, 4, 10, 10, 80, 4, 20),
    RADIATION_SUIT("Radiation Suit", 9, 5000, 15, 4, 30, 0, 30, 10, 60, 0, 20, 4, 40),
    COMBAT_ARMOR("Combat Armor", 9, 6500, 20, 5, 40, 8, 60, 4, 30, 4, 50, 6, 40),
    COMBAT_ARMOR_MKII("Combat Armor MK II", 9, 8000, 25, 6, 40, 6, 65, 5, 35, 5, 50, 9, 45);


    private String armorName;
    private double weight;
    private int value;
    private int acMod;
    private int normalDamageT, normalDamageMod;
    private int laserDamageT, laserDamageMod;
    private int fireDamageT, fireDamageMod;
    private int plasmaDamageT, plasmaDamageMod;
    private int explosionDamageT, explosionDamageMod;


    ArmorsEnum(String name, double weight, int value, int acMod,
               int normalDamageT, int normalDamageMod,
               int laserDamageT, int laserDamageMod,
               int fireDamageT, int fireDamageMod,
               int plasmaDamageT, int plasmaDamageMod,
               int explosionDamageT, int explosionDamageMod) {

        this.armorName = name;
        this.weight = weight;
        this.value = value;
        this.acMod = acMod;
        this.normalDamageT = normalDamageT;
        this.normalDamageMod = normalDamageMod;
        this.laserDamageT = laserDamageT;
        this.laserDamageMod = laserDamageMod;
        this.fireDamageT = fireDamageT;
        this.fireDamageMod = fireDamageMod;
        this.plasmaDamageT = plasmaDamageT;
        this.plasmaDamageMod = plasmaDamageMod;
        this.explosionDamageT = explosionDamageT;
        this.explosionDamageMod = explosionDamageMod;
    }

    public String getArmorName() {
        return armorName;
    }

    public double getWeight() {
        return weight;
    }

    public int getValue() {
        return value;
    }

    public int getAcMod() {
        return acMod;
    }

    public int getNormalDamageT() {
        return normalDamageT;
    }

    public int getNormalDamageMod() {
        return normalDamageMod;
    }

    public int getLaserDamageT() {
        return laserDamageT;
    }

    public int getLaserDamageMod() {
        return laserDamageMod;
    }

    public int getFireDamageT() {
        return fireDamageT;
    }

    public int getFireDamageMod() {
        return fireDamageMod;
    }

    public int getPlasmaDamageT() {
        return plasmaDamageT;
    }

    public int getPlasmaDamageMod() {
        return plasmaDamageMod;
    }

    public int getExplosionDamageT() {
        return explosionDamageT;
    }

    public int getExplosionDamageMod() {
        return explosionDamageMod;
    }
}
