package com.atolsoft.rpgtool.data;

import java.io.Serializable;
import java.util.ArrayList;

public class CharacterDatabase implements Serializable {

    private ArrayList<FalloutCharacter> characterArray;

    public ArrayList<FalloutCharacter> getCharacterArray() {
        return characterArray;
    }

    public void setCharacterArray(ArrayList<FalloutCharacter> characterArray) {
        this.characterArray = characterArray;
    }

    public CharacterDatabase() {
        characterArray = new ArrayList<>();
    }

    public void addCharacter(FalloutCharacter fChar) {
        characterArray.add(fChar);
    }


    public void printCharacterList() {
        System.out.println("Ilość postaci w bazie: " + characterArray.size() + "\n");
        for(FalloutCharacter f: characterArray) {
            System.out.println(f);
        }
    }
}