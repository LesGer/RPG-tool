package com.atolsoft.rpgtool.data.skills;

import com.atolsoft.rpgtool.data.FalloutCharacter;
import com.atolsoft.rpgtool.data.names.SkillsName;

public class Gambling extends Skills {

    public Gambling() {
        super();
        setSkillName(SkillsName.GAMBLING.getSkillName());
        setSkillKey(SkillsName.GAMBLING.getSkillkey());
    }

    @Override
    public int countSkill(FalloutCharacter fChar) {
        return (5 * getCurrentSpecial(fChar, "LK"));
    }
}
