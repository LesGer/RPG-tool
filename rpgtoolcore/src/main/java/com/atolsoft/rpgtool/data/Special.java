package com.atolsoft.rpgtool.data;

public class Special {
    private String specialName;
    private int specialValue;
    private int specialMaxValue;
    private int specialMinValue;


    public String getSpecialName() {
        return specialName;
    }

    public void setSpecialName(String specialName) {
        this.specialName = specialName;
    }

    public int getSpecialValue() {
        return specialValue;
    }

    public void setSpecialValue(int specialValue) {
        this.specialValue = specialValue;
    }

    public Special(String name){
        setSpecialName(name);
    }

    public int getSpecialMaxValue() {
        return specialMaxValue;
    }

    public void setSpecialMaxValue(int specialMaxValue) {
        this.specialMaxValue = specialMaxValue;
    }

    public int getSpecialMinValue() {
        return specialMinValue;
    }

    public void setSpecialMinValue(int specialMinValue) {
        this.specialMinValue = specialMinValue;
    }

    @Override
    public String toString() {
        return "Special{" +
                "specialName='" + specialName + '\'' +
                ", specialValue=" + specialValue +
                ", specialMaxValue=" + specialMaxValue +
                ", specialMinValue=" + specialMinValue +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Special special = (Special) o;

        if (specialValue != special.specialValue) return false;
        return specialName != null ? specialName.equals(special.specialName) : special.specialName == null;
    }

    @Override
    public int hashCode() {
        int result = specialName != null ? specialName.hashCode() : 0;
        result = 31 * result + specialValue;
        return result;
    }
}
