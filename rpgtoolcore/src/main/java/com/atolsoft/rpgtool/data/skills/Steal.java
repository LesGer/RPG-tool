package com.atolsoft.rpgtool.data.skills;

import com.atolsoft.rpgtool.data.FalloutCharacter;
import com.atolsoft.rpgtool.data.names.SkillsName;

public class Steal extends Skills {

    public Steal() {
        super();
        setSkillName(SkillsName.STEAL.getSkillName());
        setSkillKey(SkillsName.STEAL.getSkillkey());
    }

    @Override
    public int countSkill(FalloutCharacter fChar) {
        return 3 * getCurrentSpecial(fChar, "AG");
    }
}
