package com.atolsoft.rpgtool.data;

import java.util.Map;

public class Human extends FalloutCharacter {

    private static final long serialVersionUID = 1210679881108640395L;


    public Human(String name, int age, int level, int xp) {
        super(name, age, level, xp);
        setRace("Human");
        setFreeSpecialPoints(5);
        putSpecialInArray();
        specialValuesSetter();
        putSkillsInArray();
    }
    public Human() {
        setRace("Human");
        setFreeSpecialPoints(5);
        putSpecialInArray();
        specialValuesSetter();
        putSkillsInArray();
    }

    @Override
    public void specialValuesSetter(){
        for(Map.Entry<String,Special> entry : getSpecial().entrySet()){
            entry.getValue().setSpecialValue(5);
            entry.getValue().setSpecialMaxValue(10);
            entry.getValue().setSpecialMinValue(1);
        }
    }

    @Override
    public void addRaceMods(){
    }



}
