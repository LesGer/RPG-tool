package com.atolsoft.rpgtool.data.skills;

import com.atolsoft.rpgtool.data.FalloutCharacter;
import com.atolsoft.rpgtool.data.names.SkillsName;

public class SmallGuns extends Skills {


    public SmallGuns(){
        super();
        setSkillName(SkillsName.SMALL_GUNS.getSkillName());
        setSkillKey(SkillsName.SMALL_GUNS.getSkillkey());
    }

    @Override
    public int countSkill(FalloutCharacter fChar) {
        return (getCurrentSpecial(fChar,"AG")*4);
    }
}
