package com.atolsoft.rpgtool.data.skills;

import com.atolsoft.rpgtool.data.FalloutCharacter;
import com.atolsoft.rpgtool.data.names.SkillsName;

public class Sneak extends Skills {

    public Sneak() {
        super();
        setSkillName(SkillsName.SNEAK.getSkillName());
        setSkillKey(SkillsName.SNEAK.getSkillkey());
    }

    @Override
    public int countSkill(FalloutCharacter fChar) {
        return (getCurrentSpecial(fChar, "AG") * 3);
    }
}
